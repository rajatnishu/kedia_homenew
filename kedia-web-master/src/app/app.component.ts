import { Component, OnInit } from '@angular/core';
import {  NavigationCancel, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'kedia-web';

  constructor(private router: Router){

  }

  location: any;
    routerSubscription: any;

  ngOnInit(){
    this.recallJsFuntions();
  }

  recallJsFuntions() {
    this.routerSubscription = this.router.events
    .pipe(filter(event => event instanceof NavigationEnd || event instanceof NavigationCancel))
    .subscribe(event => {
        this.location = this.router.url;
    });
}
}
