import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _isAuthenticated: boolean;
  constructor(private http: HttpClient) {
    this._isAuthenticated = false;
  }

  apiUrl = environment.apiUrl;

  /**
   * Getter setter for user
   * */
  set user(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  get user() {
    return JSON.parse(localStorage.getItem('user'));
  }

  registerUser(data) {
    return this.http.post(`${this.apiUrl}/user/register`, data);
  }


  login(data) {
    debugger
    return this.http.post(`${this.apiUrl}/user/login`, data).pipe(
      switchMap((response) => {
        if (response['status'] == 'success') {

          this.user = response['data'];

          this._isAuthenticated = true;
        }

        // Return a new observable with the response
        return of(response);
      })
    );
  }

  /**
 * Sign out
 */
  signOut(): Observable<any> {
    // Remove the access token from the local storage
    localStorage.removeItem('user');

    // Set the authenticated flag to false
    this._isAuthenticated = false;

    // Return the observable
    return of(true);
  }

}
