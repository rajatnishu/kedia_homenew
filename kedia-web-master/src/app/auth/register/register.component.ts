import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidationService } from '../custom-validation.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private fb: FormBuilder, private customValidation: CustomValidationService) { }

  registrationForm: FormGroup;

  ngOnInit(): void {
    this.registrationForm = this.fb.group({
      fullName: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      password: ['', Validators.required],
      cnfPassword: ['', Validators.required],
      consent: ['', Validators.required]
    }, {
      validator: this.customValidation.MatchPassword(
        "password",
        "cnfPassword"
      ),
    })
  }


  submitRegistration() {

  }
}
