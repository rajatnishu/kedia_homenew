import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private fb: FormBuilder, private router: Router, private toast: ToastrService) { }

  loginForm: FormGroup;
  adminUrl = environment.adminUrl;

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }


  submitLogin() {
    debugger
    this.authService.login(this.loginForm.getRawValue()).subscribe(
      res => {
        if (res['status'] == 'success') {
          if (res['data'].role == 'admin') {
            window.location.href = `http://13.233.133.84:4001/?token=${res['data'].access_token}`;
            // window.location.href =  `http://localhost:4301/?token=${res['data'].access_token}`;
          } else {
            this.toast.success('You are successfully logged in.')
            this.router.navigate(['/']);
          }
        } else {
          console.log({
            res
          })
        }
      }
    )
  }
}
