import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HomeService } from '../home/home-service.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  constructor(private homeService: HomeService, private router: Router) { }

  projectsAll;
  projectNew;
  projectBest;
  activeTab = 'all'

  imageUrl = environment.imageUrl;

  ngOnInit(): void {
    let url = this.router.url;
    if (url == '/new-launch') {
      this.activeTab = 'new'
    }

    this.homeService.getProjects().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.projectsAll = res['data'];
          debugger
          this.projectNew = this.projectsAll.filter(project => {
            if (project.category == 'new') {
              return true;
            }
          });

          this.projectBest = this.projectsAll.filter(project => {
            if (project.category == 'best') {
              return true
            }
          })

          console.log({
          a:this.projectNew,
          b: this.projectsAll
          })
        } else {
          console.log({
            error: res
          })
        }
      }
    )
  }



}
