import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectsDetailPageComponent } from './projects-detail-page.component';

const routes: Routes = [
  {
    path: 'details/:projectId',
    component: ProjectsDetailPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsDetailPageRoutingModule { }
