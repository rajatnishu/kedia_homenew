import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsDetailPageComponent } from './projects-detail-page.component';

describe('ProjectsDetailPageComponent', () => {
  let component: ProjectsDetailPageComponent;
  let fixture: ComponentFixture<ProjectsDetailPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectsDetailPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
