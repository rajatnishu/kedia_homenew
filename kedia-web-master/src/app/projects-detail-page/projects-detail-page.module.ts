import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsDetailPageRoutingModule } from './projects-detail-page-routing.module';
import { ProjectsDetailPageComponent } from './projects-detail-page.component';


@NgModule({
  declarations: [ProjectsDetailPageComponent],
  imports: [
    CommonModule,
    ProjectsDetailPageRoutingModule
  ]
})
export class ProjectsDetailPageModule { }
