import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { pid } from 'process';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectDetailsService {

  constructor(private http: HttpClient) { }

  apiUrl = environment?.apiUrl;

  getProject(pId) {
    return this.http.get(`${this.apiUrl}/project/details-full/${pId}`)
  }

  getBuildings(pId){
    return this.http.get(`${this.apiUrl}/project/building/list/${pId}`);
  }

  getProperty(data){
    return this.http.post(`${this.apiUrl}/project/property/search`, data);
  }

}
