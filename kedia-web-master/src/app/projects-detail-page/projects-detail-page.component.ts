import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ProjectDetailsService } from './project-details.service';

@Component({
  selector: 'app-projects-detail-page',
  templateUrl: './projects-detail-page.component.html',
  styleUrls: ['./projects-detail-page.component.css']
})
export class ProjectsDetailPageComponent implements OnInit {

  constructor(  private  route: ActivatedRoute, private prjDetailsService: ProjectDetailsService, private router: Router) {
    this.route.params.subscribe( params => {
      console.log(params.projectId);
      const userId = this.route.snapshot.params
      this.prjDetailsService.getProject(this.route.snapshot.params.id).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.project = res['data'];
            console.table(res['data']);

            /* getting buildings of project */
            this.prjDetailsService.getBuildings(this.project._id).subscribe(
              buildRes => {
                if (buildRes['status'] == 'success') {
                  this.buildings = buildRes['data'];
                  this.activeBuilding = this.buildings[0]?._id

                  this.getProperty({
                    building: this.buildings[0]?._id,
                    floor: 1
                  });

                } else {
                  console.log({
                    building: buildRes
                  })
                }
              }
            )

            this.getProperty({
              type: 'Plot',
              project: this.project?._id
            });

          } else {
            console.error({
              res
            });
          }
        }
      )
    }
  )
  
   }

  project;
  buildings;
  plots;
  flats;

  imageUrl = environment.imageUrl;

  ngOnInit() {
 
  }

  getProperty(data) {
    /* getting flats of building */
    this.prjDetailsService.getProperty(data).subscribe(
      propRes => {
        if (propRes['status'] == 'success') {
          console.log({
            property: propRes['data']
          })

          /* for getting plot */
          if(data.type == 'Plot'){
            this.plots = propRes['data'].filter(prop => {
              if (prop.type == 'Plot') return true;
            });
            this.activatedPlot = this.plots[0]._id
            console.log({
              plots: this.plots
            })
            return
          }

          this.flats = propRes['data'].filter(flat => {
            if (flat.type == 'Flat') return true;
          });
          this.activeFlatNum = this.flats[0]._id


        }
      }
    )
  }

  bhk;
  activeOpt = 0;
  toggleConfig(opt) {
    console.log(opt);

    this.activeOpt = opt;
  }

  menu = 'overview';
  toggleMenu(option) {
    this.menu = option;
    console.log({
      menu: this.menu
    })
  }

  /* Configuration */
  activeBhkNum = 0;
  activeBhk(index) {
    this.activeBhkNum = index;
  }

  /* Getting floors of building */
  getArray(floors) {
    return Array(floors);
  }

  // building availablity
  activeBuilding;
  toggleAvailblity(buildingId) {
    this.activeBuilding = buildingId
    console.log({
      buildingId
    })

    if (buildingId != 'plot') {
      this.getProperty({
        building: buildingId
      })
    }else{
      this.getProperty({
        type: 'Plot',
        project: this.project?._id
      })
    }
  }

  activeFloorNum = 1;
  activeFloor(floorNum) {
    this.activeFlatNum = 0
    this.activeFloorNum = floorNum

    this.getProperty({
      building: this.activeBuilding,
      floor: floorNum
    })
  }

  activeFlatNum;
  activeFlat(flatNum) {
    this.activeFlatNum = flatNum
  }

  amenities : any =[]
  project1 : any =[]
  /* Activate plot */
  activatedPlot;
  activatePlot(plot) {
    this.activatedPlot = plot
  }
  step = 'Location'
  selectpro (data){
    this.step = data
    if(this.step == 'AMENITIES'){
      this.prjDetailsService.getProject(this.route.snapshot.params.id).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.project1 = res['data'];
            this.amenities  =  this.project1.amenities
            debugger
            console.table(res['data']);
        }
    })
  }
  if(this.step == "PRICE LIST"){
    
  }
  }


  goToBooking(){
    this.router.navigate(['/booking', this.activatedPlot?._id]);
    document.getElementById("bookingModalClose").click();
    // [routerLink]="['/booking', activatedPlot?._id]" routerLinkActive="router-link-active"
  }
}
