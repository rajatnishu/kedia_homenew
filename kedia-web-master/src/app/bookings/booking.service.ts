import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { pid } from 'process';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  apiUrl = environment.apiUrl

  constructor(private http: HttpClient) { }

  getPropertyDetails(pId) {
    return this.http.get(`${this.apiUrl}/project/property/detail/${pId}`);
  }


  confirmBooking(data) {
    return this.http.post(`${this.apiUrl}/booking/add`, data);
  }

  getUserBookings() {
    return this.http.get(`${this.apiUrl}/booking/my`);
  }

  getBookingDetails(id){
    return this.http.get(`${this.apiUrl}/booking/details/${id}`);
  }
}
