import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
import { CustomValidationService } from '../auth/custom-validation.service';
import { BookingService } from './booking.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private bookingService: BookingService, private authService: AuthService, private fb: FormBuilder, private customValidation: CustomValidationService, private toast: ToastrService) { }

  property;
  imageUrl = environment.imageUrl;
  user;

  personalInfoForm: FormGroup;
  bookingId;

  ngOnInit(): void {

    this.user = this.authService.user;

    this.route.params.subscribe(
      param => {
        this.bookingService.getPropertyDetails(param.propertyId).subscribe(
          res => {
            if (res['status'] == 'success') {
              this.property = res['data']
            } else {
              console.log({
                a: res['error']
              })
            }
          }
        )
      }
    )

    this.initForm(this.user);
  }

  initForm(data) {
    this.personalInfoForm = this.fb.group({
      fullName: [data?.fullName, Validators.required],
      email: [data?.email, Validators.required],
      phone: [data?.phone, Validators.required],
      password: [data?.password, Validators.required],
      cnfPassword: [data?.cnfPassword, Validators.required],
      city: [data?.city, Validators.required],
      aadhar: [data?.aadhar, Validators.required],
      isdCode: [data?.isdCode, Validators.required]
    }, {
      validator: this.customValidation.MatchPassword(
        "password",
        "cnfPassword"
      ),
    })
  }

  paymentOpt = 'Credit';
  openPayment(option) {
    this.paymentOpt = option
  }

  activeOpt;
  toggleActive(option) {
    this.activeOpt = option
  }


  aadharFile;
  aadharPreview
  uploadImage(files, type) {
    this.aadharFile = files[0]

    const reader = new FileReader();
    reader.readAsDataURL(files[0]);

    reader.onload = event => {
      this.aadharPreview = reader.result;
    }
  }

  registerUser() {
    const formData = new FormData();
    formData.append('fullName', this.personalInfoForm.value.fullName);
    formData.append('email', this.personalInfoForm.value.email);
    formData.append('phone', this.personalInfoForm.value.phone);
    formData.append('password', this.personalInfoForm.value.password);
    formData.append('cnfPassword', this.personalInfoForm.value.cnfPassword);
    formData.append('city', this.personalInfoForm.value.city);
    formData.append('aadharCard', this.aadharFile);
    formData.append('isdCode', this.personalInfoForm.value.isdCode);

    if (!this.aadharFile) {
      this.toast.error('Aadhar card is required.');
      return
    }

    this.authService.registerUser(formData).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.toast.success('User registerd successfully, please continue to payment');
          this.toggleActive('payment');
          document.getElementById('goToPaymentBtn').click();

          this.authService.user(res['data']);
        } else {
          this.toast.error(res['error']);
        }
      }
    )
  }

  booked = false;
  /* do payemnt and confirm booking */
  makePayment() {
    // this.authService.makePayment()

    let bookingData = {
      name: this.user.fullName,
      phone: this.user.phone,
      property: this.property._id,
      paidAmount: this.property.bookingPrice
    }

    this.bookingService.confirmBooking(bookingData).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.toast.success('Your booking has been done successfully.');
          document.getElementById('finalStepBtn').click();
          this.booked = true;
          this.bookingId = res['data']._id
        } else {
          this.toast.error(res['error']);
        }
      }
    )
  }

}
