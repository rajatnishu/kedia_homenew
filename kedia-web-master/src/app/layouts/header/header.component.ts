import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { HomeService } from '../../home/home-service.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService,private homeService: HomeService, private router: Router ,private toast: ToastrService) { }

  user;
  projectsAll;
  projectVillas;
  projectFlats;
  projectPlots;
  ngOnInit(): void {
    this.user = this.authService.user
    console.log({  user: this.user})

    this.homeService.getProjects().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.projectsAll = res['data'];
          
          this.projectVillas = this.projectsAll.filter(project => {
            if (project.category == 'Villas') {
              return true;
            }
          });
          
          this.projectFlats = this.projectsAll.filter(project => {
            if (project.category == 'Flats') {
              return true
            }
          })
          
          this.projectPlots = this.projectsAll.filter(project => {
            if (project.category == 'Plots') {
              return true
            }
          })

          console.log({
          b: this.projectsAll
          })
        } else {
          console.log({
            error: res
          })
        }
      }
    )
  }

  logout() {
    this.authService.signOut();
    this.user = this.authService.user;
    this.toast.info('You are logged out successfully.');
  }

  gotopage(data) {
    
        this.router.navigate(['/projects-detail/' + data._id], { queryParams: { name: data._id } })
      }

}
