import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { BookingService } from '../bookings/booking.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  constructor(private bookingService: BookingService, private route: ActivatedRoute) { }

  booking;
  imageUrl = environment.imageUrl;

  ngOnInit(): void {
    this.route.params.subscribe(
      res => {
        this.bookingService.getBookingDetails(res.bookingId).subscribe(
          resBook => {
            if (resBook['status'] == 'success') {
              this.booking = resBook['data'];
              setTimeout(() => {
                window.print();
              }, 1000);
            }
          }
        );
      }
    )
    // this.bookingService.getBookingDetails().subscribe()
  }

}
