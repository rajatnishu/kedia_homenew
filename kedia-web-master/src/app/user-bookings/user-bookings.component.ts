import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
const { table } = console;
import { BookingService } from '../bookings/booking.service';

@Component({
  selector: 'app-user-bookings',
  templateUrl: './user-bookings.component.html',
  styleUrls: ['./user-bookings.component.css']
})
export class UserBookingsComponent implements OnInit {

  constructor(private bookingService: BookingService) { }

  bookings;
  imageUrl = environment.imageUrl;

  ngOnInit(): void {
    this.bookingService.getUserBookings().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.bookings = res['data'];
          table(res['data'])
        } else {
          console.log({
            res
          })
        }
      }
    )
  }

}
