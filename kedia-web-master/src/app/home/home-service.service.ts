import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.apiUrl

  getProjects() {
    return this.http.get(`${this.apiUrl}/project/list-full-details`);
  }

  getImages() {
    return this.http.get(`${this.apiUrl}/images/list`)
  }
}
