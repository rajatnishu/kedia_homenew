import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { HomeService } from './home-service.service';
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ProjectsResolver implements Resolve<any> {

  constructor(private homeService: HomeService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.homeService.getProjects()
    .pipe(
      catchError((error) => {
        console.log({
          error
        })

        // Throw an error
        return throwError(error);
      })
    )
  }

}
