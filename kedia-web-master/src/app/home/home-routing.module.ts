import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { ProjectsResolver } from './home.resolvers';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    // resolve: {
    //   projects: ProjectsResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
