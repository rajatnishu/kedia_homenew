import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HomeService } from './home-service.service';
import { OwlOptions } from 'ngx-owl-carousel-o';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private homeService: HomeService) { }

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      500: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    }
  }

  customOptions2: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      500: {
        items: 5
      },
      740: {
        items: 5
      },
      940: {
        items: 5
      }
    }
  }

  projectsList;
  imageUrl = environment.imageUrl;

  bestProjects;
  newProjects;
  plotsProjects;

  images;

  ngOnInit(): void {
    this.homeService.getProjects().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.projectsList = res['data'];
          debugger
          this.bestProjects = this.projectsList.filter(project => {
            if (project?.category == 'Flats') return true
          })
          this.newProjects = this.projectsList.filter(project => {
            if (project?.category == "Villas") return true
          })
          this.plotsProjects = this.projectsList.filter(project => {
            if (project?.category == "Plots") return true
          })
        } else {
          console.log({
            res
          });
        }
      }
    )

    this.homeService.getImages().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.images = res['data'];
        } else {
          console.log({
            res
          })
        }
      }
    )
  }


  activeImg = 1;
  activeSlider(index) {
    this.activeImg = index;
  }

  /* projects slider */
  activeProject = 1;
  activeNewProjects(index) {
    debugger
    this.activeProject = index;
  }
}
