import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from './auth/auth.guard';
import { BookingsComponent } from './bookings/bookings.component';
import { ContactComponent } from './contact/contact.component';
import { FaqComponent } from './faq/faq.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { NewLaunchComponent } from './new-launch/new-launch.component';
import { ProjectComponent } from './project/project.component';
import { ProjectsDetailPageComponent } from './projects-detail-page/projects-detail-page.component';
import { UserBookingsComponent } from './user-bookings/user-bookings.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'projects-detail-page/:id',
    component:ProjectsDetailPageComponent
    // loadChildren: () =>
    //   import('./projects-detail-page/projects-detail-page.module').then(
    //     (m) => m.ProjectsDetailPageModule
    //   ),
  },

  {
    path: 'projects-detail/:id',
    component:ProjectDetailComponent
  },

  {
    path: 'about',
    component: AboutComponent,
  },
  {
    path: 'new-launch',
    component: ProjectComponent
  },
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'project',
    component: ProjectComponent
  },
  {
    path: 'booking/:propertyId',
    component: BookingsComponent
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'user/bookings',
    component: UserBookingsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'print-invoice/:bookingId',
    component: InvoiceComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
