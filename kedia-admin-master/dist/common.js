(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "PiXH":
/*!****************************************************!*\
  !*** ./src/app/pages/projects/projects.service.ts ***!
  \****************************************************/
/*! exports provided: ProjectsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsService", function() { return ProjectsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "tk/3");






class ProjectsService {
    /**
     *
     * @param http http client
     */
    constructor(http) {
        this.http = http;
        this.apiUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiUrl;
        this._project = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](null);
        this._projects = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](null);
    }
    /* Getter for single project */
    get project$() {
        return this._project.asObservable();
    }
    /* Getter for projects */
    get projects$() {
        return this._projects.asObservable();
    }
    /* Api call for getting projects and setting them */
    getProjects() {
        return this.http.get(`${this.apiUrl}/project/list-full-details`).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])((projects) => {
            this._projects.next(projects['data']);
        }));
    }
    /* Adding new project */
    addNewProject(data) {
        return this.projects$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])((projects) => this.http.post(`${this.apiUrl}/project/add`, data).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])((newProject) => {
            // Update the contacts with the new contact
            this._projects.next([newProject['data'], ...projects]);
            debugger;
            // Return the new contact
            return newProject;
        }))));
    }
    /* For updating project */
    updateProject(projectId, data) {
        return this.projects$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])((projects) => this.http.post(`${this.apiUrl}/project/update/${projectId}`, data).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])((updatedProject) => {
            const index = projects.findIndex(item => item._id == projectId);
            projects[index] = updatedProject['data'];
            this._project.next(projects);
            return updatedProject;
        }))));
        // return this.http.post(`${this.apiUrl}/project/update/${projectId}`, data)
    }
    deleteProject(id) {
        return this.projects$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(projects => this.http.delete(`${this.apiUrl}/project/delete/${id}`).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])((isDeleted) => {
            // Find the index of the deleted project
            const index = projects.findIndex(item => item._id === id);
            // Delete the project
            projects.splice(index, 1);
            // Update the contacts
            this._projects.next(projects);
            // Return the deleted status
            return isDeleted;
        }))));
    }
    getProjectById(pId) {
        if (!this._projects) {
            return this.http.get(`${this.apiUrl}/project/details/${pId}`);
        }
        return this._projects.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])((projects) => {
            // finding the project
            const project = projects.find(item => item._id === pId);
            // udpate the project
            this._project.next(project);
            /* return the selected project */
            return project;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])((project) => {
            if (!project) {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["throwError"])('Could not found project with id of ' + pId + '!');
            }
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(project);
        }));
    }
    getBuildingsOfProject(pid) {
        return this.http.get(`${this.apiUrl}/project/building/list/${pid}`);
    }
    getPorpertiesOfProject(data) {
        return this.http.post(`${this.apiUrl}/project/property/list`, {
            "project": data.project,
            "building": data.building,
            "floor": data.floor,
            "type": data.type
        });
    }
    addNewBuilding(data, editMode, bId) {
        if (editMode) {
            return this.http.post(`${this.apiUrl}/project/building/update/${bId}`, data);
        }
        else {
            return this.http.post(`${this.apiUrl}/project/building/add`, data);
        }
    }
    addFlatOrPlot(data) {
        return this.http.post(`${this.apiUrl}/project/property/add`, data);
    }
    /* Deleting a building */
    deleteBuilding(bId) {
        return this.http.delete(`${this.apiUrl}/project/building/delete/${bId}`);
    }
    /* Deleting a  plot */
    deletePlot(pId) {
        return this.http.delete(`${this.apiUrl}/project/property/delete/${pId}`);
    }
    /* For changing published status  */
    changeStatus(projectId, type) {
        return this.http.get(`${this.apiUrl}/project/change-status/${type}/${projectId}`);
    }
    getBanks() {
        return this.http.get(`${this.apiUrl}/bank/list`);
    }
    getAmenities() {
        return this.http.get(`${this.apiUrl}/amenities/list`);
    }
}
ProjectsService.ɵfac = function ProjectsService_Factory(t) { return new (t || ProjectsService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"])); };
ProjectsService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ProjectsService, factory: ProjectsService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProjectsService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] }]; }, null); })();


/***/ })

}]);
//# sourceMappingURL=common.js.map