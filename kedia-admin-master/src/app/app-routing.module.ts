import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertiesResolver } from './pages/properties/property.resolvers';
import { UsersListComponent } from './pages/users-list/users-list.component';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'projects',
    loadChildren: () => import('./pages/projects/projects.module').then(m => m.ProjectsModule),
    resolve: {
      properties: PropertiesResolver
    }
  },
  {
    path: 'properties',
    loadChildren: () => import('./pages/properties/properties.module').then(m => m.PropertiesModule)
  },
  {
    path: 'buildings',
    loadChildren: () => import('./pages/buildings/buildings.module').then(m => m.BuildingsModule)
  },
  {
    path: 'banks',
    loadChildren: () => import('./pages/bank-details/bank-details.module').then(m => m.BankDetailsModule)
  },
  {
    path: 'amenities',
    loadChildren: () => import('./pages/amenities/amenities.module').then(m => m.AmenitiesModule)
  },
  {
    path: 'top-slider',
    loadChildren: () => import('./pages/top-slider/top-slider.module').then(m => m.TopSliderModule)
  },
  {
    path: 'bookings',
    loadChildren: () => import('./pages/bookings/bookings.module').then(m => m.BookingsModule)
  },
  {
    path: 'user-list',
    component: UsersListComponent
  },
  {
    path: '**',
    redirectTo: 'projects'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
