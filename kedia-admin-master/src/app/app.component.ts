import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from './pages/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'kedia-admin';

  constructor(private route: ActivatedRoute, private authService: AuthService){}

  ngOnInit(){
    // this.route.queryParams.subscribe(
    //   res => {
    //     console.log({
    //       res
    //     })
    //     debugger
    //     this.authService.accessToken = res.token;
    //   }
    // )
  }
}
