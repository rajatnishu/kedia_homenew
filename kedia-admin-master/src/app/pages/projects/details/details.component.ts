import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ProjectsService } from '../projects.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  private _unsubscribeAll: Subject<any>;

  imageUrl = environment.imageUrl;

  project: any;

  constructor(
    private projectService: ProjectsService,
    private toast: ToastrService,
    private route: ActivatedRoute
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {

    this.getProjects();
    this.getBuildings();
    this.getPlots();
  }

  buildings = [];
  getBuildings() {
    if (this.buildings.length != 0) {
      return
    }
    this.projectService.getBuildingsOfProject(this.project._id).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.buildings = res['data']
        } else {
          console.log({
            res
          })
        }
      }
    )
  }

  plots = [];
  getPlots() {
    if (this.plots.length != 0) {
      return
    }

    let data = {
      project: this.project._id,
      building: '',
      floor: '',
      type: 'Plot'
    }
    this.projectService.getPorpertiesOfProject(data).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.plots = res['data']
        } else {
          console.log({
            res
          })
        }
      }
    )
  }


  flats = [];
  getFlats() {
    if (this.flats.length != 0) {
      return
    }

    let data = {
      project: this.project._id,
      building: '',
      floor: '',
      type: 'Flat'
    }
    this.projectService.getPorpertiesOfProject(data).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.flats = res['data']
        } else {
          console.log({
            res
          })
        }
      }
    )
  }


  getProjects() {

    this.projectService.project$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((project) => {

        this.project = project

        console.log({
          project
        })

        this.building.project = project._id
        this.flatDetails.project = project._id
        this.plotDetails.project = project._id
      })
  }

  /* Asking for the entity details */
  addType

  /* plot details */
  plot = {

  }

  /* Building details */
  building = {
    name: '',
    description: '',
    floors: '',
    flats: '',
    lifts: '',
    serviceLift: '',
    project: '',
    images: [],
    _id: ''
  }

  addPlot = false;
  addFlats = false;

  addEntity() {

    console.log({
      type: this.addType
    })

    /* ---------------------- Common for both plot and building */
    const formData = new FormData();

    if (this.images.length > 0) {
      this.images.forEach((element, i) => {
        formData.append('images', element);
      });
    }
    console.log({
      planImage: this.planImage,
      planLength: this.planImage?.length
    })
    if (this.planImage) {
      formData.append('planImage', this.planImage)
    }



    if (this.addType == 'Building') {
      if (this.editBuilding)
        this.building._id = undefined
      delete this.building._id

      /* Adding building */
      for (var key in this.building) {
        formData.append(key, this.building[key]);
      }

      this.projectService.addNewBuilding(formData, this.editBuilding, this.building._id).subscribe(
        res => {
          if (res['status'] == 'success') {

            this.toast.success(`Building ${this.editBuilding ? 'updated' : 'added'} successfully.`);

            /* if we are udating */
            if (this.editBuilding) {
              document.getElementById("closeModal").click()
            } else {

              /* asking to add flats in building */
              this.addFlats = !this.addFlats

              /* setting building id in building obj */
              this.building['id'] = res['data']._id

              /* setting build ing id in flat obj */
              this.flatDetails.building = res['data']._id

              // this.buildings.push(res['data']);
              this.getProjects();

              this.addType = 'flat'
            }
            this.getBuildings();

          } else {
            console.log({
              res
            });
            if (res['error']) {
              this.toast.error(res['error'])
            } else {
              this.toast.error('Some error occoured')
            }
          }
        }
      );

    } else {

      for (var key in this.plotDetails) {
        formData.append(key, this.plotDetails[key]);
      }

      formData.append('images360', this.images360)
      // formData.append('project', this.project?._id);

      this.projectService.addFlatOrPlot(formData).subscribe(
        res => {
          if (res['status'] == 'success') {

            this.toast.success('Plot added successfully.');

            /* asking to add flats in building */
            this.addPlot = !this.addPlot

            this.plots.push(res['data']);

            this.getPlots();

            document.getElementById("closeModal").click()
          } else {
            console.log({
              res
            });
            if (res['error']) {
              this.toast.error(res['error'])
            } else {
              this.toast.error('Some error occoured')
            }
          }
        }
      )

      /* show add plot with similar and different */
      this.addPlot = !this.addPlot
    }
  }

  flatDetails = {
    type: 'Flat',
    description: '',
    areaSqft: '',
    serial: '',
    bedrooms: '',
    bathrooms: '',
    livingRoom: '',
    balcony: '',
    price: 0,
    bookingPrice: 0,
    floor: '',
    building: '',
    project: '',
  }

  plotDetails = {
    type: 'Plot',
    description: '',
    areaSqft: '',
    serial: '',
    bedrooms: '',
    bathrooms: '',
    livingRoom: '',
    balcony: '',
    price: 0,
    bookingPrice: 0,
    project: '',
    possessionStatus: '',
    perSqFtPrice: '',
  }

  submitFlat() {
    this.projectService.addFlatOrPlot(this.addFlats ? this.flatDetails : this.plotDetails).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.toast.success(`${this.addFlats == true ? 'Flat' : 'Plot'} added successfuly.`)
        } else {

          if (res['error']) {
            this.toast.error(res['error'])
          } else {
            this.toast.error('Some error occoured')
          }

        }
      }
    )
  }

  planImage;

  uploadPlanImage(file) {

    this.planImage = file[0]
    console.log({
      file,
      pi: this.planImage
    })
  }

  images = [];
  uploadImages(files) {
    this.images = Array.from(files)
    console.log({
      files,
      i: this.images
    })
  }

  images360;
  upload360Images(files) {
    this.images360 = files[0]
  }


  deleteBuilding(bId, index) {
    if (confirm('Are you sure want to delete this building ?')) {
      this.projectService.deleteBuilding(bId).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.buildings.splice(index, 1);
            this.toast.success('Building deleted from project successfully.')
          } else {
            this.toast.error(res['error'])
          }
        }
      )
    }
  }

  editBuilding = false;
  getBuildingForEdit(bId) {
    this.building = this.buildings.find(b => b._id === bId);

    document.getElementById('addBuildingProj').click();

    this.addType = 'Building'

    this.editBuilding = true;

    console.log({
      b: this.building
    })
  }

  /* --------------------------------------------------deleting a plot------------------------------------------------------------------- */
  deletePlot(pId, index) {
    if (confirm('Are you sure want to delete this plot ?')) {
      this.projectService.deletePlot(pId).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.plots.splice(index, 1);
            this.toast.success('Plot deleted from project successfully.')
          } else {
            this.toast.error(res['error'])
          }
        }
      )
    }
  }


  /* -------------------------------------------------------------For making duplicate building------------------------------------------ */
  selectedBuilding; // for duplicating
  setBuilding(building) {
    this.selectedBuilding = building;
  }

  /* Setting duplicate data to submit*/
  data = {
    buildingName: []
  }

  /* creating Array according to input for iteration */
  numOfDuplicate: number = 1;
  duplicateArray;
  setDuplicate(value) {
    this.numOfDuplicate = value;
    this.duplicateArray = Array(+this.numOfDuplicate);
  }


  /* For building */
  submitDuplicate() {
    console.log({
      data: this.data
    })

    if (this.data.buildingName.length != +this.numOfDuplicate) {
      this.toast.error('Please fill all fields')
      return
    }


    /*  creating form data to submit */
    this.data.buildingName.forEach((element, index) => {
      debugger
      const formData = new FormData();

      formData.append('name', element);
      formData.append('description', this.selectedBuilding.description);
      formData.append('floors', this.selectedBuilding.floors);
      formData.append('flats', this.selectedBuilding.flats);
      formData.append('lifts', this.selectedBuilding.lifts);
      formData.append('serviceLift', this.selectedBuilding.serviceLift);
      formData.append('project', this.selectedBuilding.project);

      this.selectedBuilding.images.forEach(img => {
        formData.append('images', img);
      });


      this.projectService.addNewBuilding(formData, this.editBuilding).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.buildings.push(res['data']);

            document.getElementById("close").click();
          } else {
            console.log({
              res
            })
            this.toast.error(res['error'])
          }
        }
      )

    });
  }

  /* ---------------------------------------------For making plot duplicate-------------------------------------------------------------- */


  plotData = {
    serial: []
  }

  selectedPlot;
  setPlot(plot) {
    this.selectedPlot = plot;
    console.log({
      plot
    })
  }

  submitDuplicatePlots() {
    console.log({
      image: this.selectedPlot.images
    })


    if (this.plotData.serial.length != this.numOfDuplicate) {
      this.toast.error('Please fill all the details.')
    }

    this.plotData.serial.forEach((element, index) => {

      const formData = new FormData();

      formData.append('type', this.selectedPlot.type);
      formData.append('description', this.selectedPlot.description);
      formData.append('areaSqft', this.selectedPlot.areaSqft);
      formData.append('serial', element);
      formData.append('bedrooms', this.selectedPlot.bedrooms);
      formData.append('bathrooms', this.selectedPlot.bathrooms);
      formData.append('livingRoom', this.selectedPlot.livingRoom);
      formData.append('balcony', this.selectedPlot.balcony);
      formData.append('price', this.selectedPlot.price);
      formData.append('bookingPrice', this.selectedPlot.bookingPrice);
      formData.append('project', this.selectedPlot.project);

      this.selectedPlot.images.forEach(img => {
        formData.append('images', img);
      });

      this.projectService.addFlatOrPlot(formData).subscribe(
        res => {
          if (res['status'] == 'success') {

            this.toast.success('Plot added successfully.');

            this.plots.push(res['data']);

            document.getElementById("closeDupliPlot").click()
          } else {
            console.log({
              res
            });
            if (res['error']) {
              this.toast.error(res['error'])
            } else {
              this.toast.error('Some error occoured')
            }
          }
        }
      )
    })

    this.getPlots();
  }

  changeStatus(projectId) {
    this.projectService.changeStatus(projectId, 'project').subscribe(
      res => {
        if (res['status'] == 'success') {
          this.toast.success('Project\'s status changed successfully');

          this.project.published = !this.project.published;
        } else {
          console.log({
            res
          })
          this.toast.error('Something went wrong')
        }
      }
    )
  }
}
