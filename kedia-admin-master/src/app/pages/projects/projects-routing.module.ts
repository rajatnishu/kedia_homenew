import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PropertiesResolver } from '../properties/property.resolvers';
import { DetailsComponent } from './details/details.component';
import { ListComponent } from './list/list.component';

import { ProjectsComponent } from './projects.component';
import { ProjectsProjectResolver, ProjectsResolver, TokenResolver } from './projects.resolvers';

const routes: Routes = [
  {
    path: '',
    component: ProjectsComponent,
    children: [
      {
        path: '',
        component: ListComponent,
        resolve: {
          projects: ProjectsResolver,
          token: TokenResolver
        }
      },
      {
        path: ':projectId',
        component: DetailsComponent,
        resolve: {
          projectDetails: ProjectsProjectResolver
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
