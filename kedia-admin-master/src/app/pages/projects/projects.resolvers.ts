import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { ProjectsService } from './projects.service';

@Injectable({
  providedIn: 'root'
})
export class TokenResolver implements Resolve<any>{
  constructor(private authService: AuthService){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    let tokenFound = this.authService.accessToken;
    debugger
    if (!tokenFound) {
      this.authService.accessToken = route.queryParams['token'];

    }
  }
}


@Injectable({
  providedIn: 'root'
})
export class ProjectsResolver implements Resolve<any>{

  constructor(
    private projectService: ProjectsService
  ) { }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    return this.projectService.getProjects();
  }
}


@Injectable({
  providedIn: 'root'
})
export class ProjectsProjectResolver implements Resolve<any>{

  constructor(
    private projectService: ProjectsService,
    private router: Router
  ) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    console.log({
      route: route.paramMap.get('projectId')
    })
    return this.projectService.getProjectById(route.paramMap.get('projectId'))
      .pipe(
        catchError((error) => {
          console.log({
            error
          })

           // Get the parent url
          //  const parentUrl = state.url.split('/').slice(0, -1).join('/');

           // Navigate to there
          //  this.router.navigateByUrl(parentUrl);

           // Throw an error
           return throwError(error);
        })
      )

  }
}

// @Injectable({
//   providedIn: 'root'
// })
// export class CountriesResolver implements Resolve<any>{


//   constructor(private authService: AuthService) {

//   }

//   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
//     return this.authService.get
//   }
// }
