import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { pid } from 'process';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ProjectsService } from '../projects.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  projects$: Observable<any>;
  projectsCount: number;

  imageUrl = environment.imageUrl;

  /* Private */
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private projectService: ProjectsService,
    private router: Router,
    private toast: ToastrService
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();

    this.projectsCount = 0;
  }

  viewList = true


  addProjectForm;

  banksList;
  amenities

  /* for config */
  option = {
    size: '',
    price: '',
    builtUpArea: '',
    image: ''
  }
  optionsArray = [
    this.option
  ]
  bhk = {
    name: '',
    options: [
      this.option
    ]
  }


  ngOnInit(): void {
    this.projects$ = this.projectService.projects$;

    this.projectService.projects$
      .pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (projects: any[]) => {
          this.projectsCount = projects?.length | 0
        }
      )

    this.initForm();

    this.projectService.getBanks().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.banksList = res['data'];
        } else {
          console.log({
            res
          })
        }
      }
    )

    this.projectService.getAmenities().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.amenities = res['data'];
        } else {
          console.log({
            res
          })
        }
      }
    )
  }

  toggleListGridView() {
    this.viewList = !this.viewList
  }


  initForm(data?) {
    /* Add / Update projet Form */
    this.addProjectForm = this.fb.group({
      name: [data?.name, Validators.required],
      description: [data?.description],
      amenities: [data?.amenities, Validators.required],
      address: [data?.address, Validators.required],
      numberOfBuildings: [data?.numberOfBuildings, Validators.required],
      numberOfHouses: [data?.numberOfHouses, Validators.required],
      city: [data?.city, Validators.required],
      state: [data?.state, Validators.required],
      images: [''],
      logo: [''],
      latitude: [24],
      longitude: [24],

      area: [data?.area, Validators.required],
      launchDate: [data?.launchDate, Validators.required],
      reraId: [data?.reraId, Validators.required],
      bankDetails: [data?.bankDetails],
      startsFrom: [data?.startsFrom, Validators.required],

      category: [data?.category, Validators.required],
      keyfeatures: [data?.keyfeatures, Validators.required],
      whyInvest: [data?.whyInvest, Validators.required],
      floorings: [data?.floorings, Validators.required],
      fittings: [data?.fittings, Validators.required],
      walls: [data?.walls, Validators.required],

      avgPrice: [data?.avgPrice, Validators.required],
      typesOfFlats: [data?.typesOfFlats, Validators.required],
    })
  }

  projectImages = [];
  previewImages = [];
  projectLogo = [];
  projectPlan;
  projectBroucher;
  valid;
  height
  width
  uploadImages(files, type) {

    if (files.length > 0) {

      if (type == 'images') {
        this.projectImages = Array.from(files);

        this.projectImages.forEach(image => {
          const reader = new FileReader();
          reader.readAsDataURL(image);

          let This = this;
          reader.onload = event => {
            this.previewImages.push(reader.result);

            var image = new Image();
            image.src = event.target.result.toString();
            let self = image;
            image.onload = function () {
              var height = self.height;
              var width = self.width;

              console.log({
                height,
                width
              });

              if (width < 263 || height < 263) {
                This.valid = false;
                This.projectImages = null;
                This.previewImages = null;
                alert('Please select image with width 263 and height 263 or above');
              }
              document.getElementById("widthProject").innerHTML = width.toString();
              document.getElementById("heightProject").innerHTML = height.toString();
            }
          };
        });
      } else if (type == 'logo') {

        this.projectLogo[0] = files[0];

        const reader = new FileReader();
        reader.readAsDataURL(files[0]);

        let This = this;
        reader.onload = event => {
          this.projectLogo[1] = reader.result;

          var image = new Image();
          image.src = event.target.result.toString();
          let self = image;
          image.onload = function () {
            var height = self.height;
            var width = self.width;

            console.log({
              height,
              width
            });

            if (width < 263 || height < 263) {
              This.projectLogo = [];
              alert('Please select image with width 263 and height 263 or above');
            }
            document.getElementById("widthLogo").innerHTML = width.toString();
            document.getElementById("heightLogo").innerHTML = height.toString();
          }

        };

        console.log({
          logo: this.projectLogo
        })

      } else if (type == 'plan') {

        this.projectPlan = files[0]

      } else if (type == 'broucher') {

        this.projectBroucher = files[0]

      }

    }
  }


  /* For removing images from array */
  removeImage(index) {
    this.previewImages.splice(index, 1)
    this.projectImages.splice(index, 1)
    this.uploadedPreviewImages.splice(index, 1)
  }

  /* For adding/updating a project */
  addProject() {
    console.log({
      data: this.addProjectForm
    })

    if (this.projectImages.length == 0 && !this.editMode) {
      this.toast.error('At least 3 project images are required');
      return
    }

    if (this.projectLogo.length == 0) {
      this.toast.error('Please upload project logo');
      return
    }

    const formData = new FormData();

    let data = this.addProjectForm.getRawValue();
    for (var key in data) {
      if (key == 'bankDetails' && data[key].length > 0) {
        let banks = data[key];
        banks.forEach(element => {
          formData.append(key, element)
        });

      } else if (key == 'amenities' && data[key].length > 0) {
        debugger
        let amenities = data[key];
        amenities.forEach(element => {
          formData.append(key, element)
        });
      } else {
        formData.append(key, data[key]);
      }
    }

    if (this.projectImages?.length > 0) {
      this.projectImages.forEach((element, i) => {
        formData.append('images', element);
      });
    }

    /* appending projcet logo*/
    if (this.projectLogo.length > 0) {
      formData.append('logo', this.projectLogo[0]);
    }

    /* for the images that are removed */
    if (this.uploadedPreviewImages?.length > 0) {
      this.uploadedPreviewImages.forEach((element, i) => {
        formData.append('deleteImages', element);
      });
    }

    /* appending project plan image */
    if (this.projectPlan) {
      formData.append('planMap', this.projectPlan)
    }

    /* appending project broucher */
    if (this.projectBroucher) {
      formData.append('broucher', this.projectBroucher)
    }

    console.log({
      data: this.bhkArray
    })

    formData.append('bhkNums', JSON.stringify(this.bhkArray))


    if (!this.editMode) {

      this.projectService.addNewProject(formData).subscribe(
        res => {

          if (res['status'] == 'success') {
            document.getElementById("closeModal").click();
            this.toast.success('Project added successfully.');
            this.projectImages, this.previewImages, this.uploadedPreviewImages = [];
            this.initForm();
            this.projectService.getProjects();
            this.router.navigate(['/projects', res['data']._id])
          } else {
            this.toast.error(res['error']);
          }
        }
      )
    } else {

      this.projectService.updateProject(this.projectId, formData).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.toast.success("Project updated successfully.");
            document.getElementById("closeModal").click();
            this.editMode = false

            this.projectImages, this.previewImages, this.uploadedPreviewImages = [];
            this.initForm();

            this.projectService.getProjects();
          } else {
            this.toast.error(res['error']);
          }
        }
      )
    }
  }


  projectDetails(pId) {
    this.router.navigate(['/projects', pId]);
  }


  deleteProject(pId) {
    if (confirm("Are you sure want to delete this project ?")) {
      this.projectService.deleteProject(pId).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.toast.success("Project deleted successfully.")
          } else {
            this.toast.error(`${res['error']}`)
          }
        }
      )
    }
  }

  /* for checking if it is edit mode or not */
  editMode = false;
  projectId;
  uploadedPreviewImages;
  getProjectForEdit(pId) {
    this.projectId = pId;
    this.projectService.getProjectById(pId).subscribe(
      projData => {

        document.getElementById("addProjectBtn").click();
        console.log({
        projData
        })
        this.initForm(projData)

        this.editMode = true;

        this.uploadedPreviewImages = projData['images'];
        this.projectLogo[1] = projData['logo'][0];
        this.projectPlan = projData['planMap'];
        this.projectBroucher = projData['broucher'];

        let amenitites = []
        projData?.amenities.filter(ame => {
          amenitites.push(ame?._id)
        })
        debugger
        this.addProjectForm.value('amenities', amenitites);
debugger
        (!projData['bhkNums'] || projData['bhkNums'].length < 1) ? this.bhkArray = [this.bhk] : this.bhkArray = projData['bhkNums'];

        console.log({
          previewImagesOnEdit: this.uploadedPreviewImages,
          bhkArr: this.bhkArray,
          projData
        })

      }
    )
  }

  /* For configuration */
  bhkArray = [this.bhk]
  addOptions(type, index?) {
    console.log({ type, index })
    let optionArray = this.bhkArray[index].options;
    type == 'add' ? optionArray.push({
      size: '',
      price: '',
      builtUpArea: '',
      image: ''
    }) : optionArray.splice(index, 1)

    console.log({
      a: this.bhkArray
    })
  }


  addBhk(type, index?) {
    console.log({ type, index })
    type == 'add' ? this.bhkArray.push({
      name: '',
      options: [
        {
          size: '',
          price: '',
          builtUpArea: '',
          image: ''
        }
      ]
    }) : this.bhkArray.splice(index, 1);
  }



  configImg(bhkIndex, optionIndex, files) {

    const reader = new FileReader()
    reader.readAsDataURL(files[0]);

    reader.onload = event => {
      this.bhkArray[bhkIndex].options[optionIndex].image = reader.result.toString();
    }

  }
}
