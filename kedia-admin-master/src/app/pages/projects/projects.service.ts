import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { pid } from 'process';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  private _project: BehaviorSubject<any>
  private _projects: BehaviorSubject<any[]>;


  apiUrl = environment.apiUrl

  /**
   *
   * @param http http client
   */
  constructor(private http: HttpClient) {
    this._project = new BehaviorSubject(null);
    this._projects = new BehaviorSubject(null);
  }

  /* Getter for single project */
  public get project$(): Observable<any> {
    return this._project.asObservable();
  }

  /* Getter for projects */
  public get projects$(): Observable<any> {
    return this._projects.asObservable()
  }


  /* Api call for getting projects and setting them */
  getProjects(): Observable<any> {
    return this.http.get(`${this.apiUrl}/project/list-full-details`).pipe(
      tap((projects) => {
        this._projects.next(projects['data']);
      })
    );
  }

  /* Adding new project */
  addNewProject(data) {

    return this.projects$.pipe(
      take(1),
      switchMap((projects) => this.http.post<any>(`${this.apiUrl}/project/add`, data).pipe(
        map((newProject) => {

          // Update the contacts with the new contact
          this._projects.next([newProject['data'], ...projects]);

          debugger;

          // Return the new contact
          return newProject;
        })
      ))
    );
  }


  /* For updating project */
  updateProject(projectId, data) {
    return this.projects$.pipe(
      take(1),
      switchMap((projects) => this.http.post<any>(`${this.apiUrl}/project/update/${projectId}`, data).pipe(
        map((updatedProject) => {

          const index = projects.findIndex(item => item._id == projectId);

          projects[index] = updatedProject['data'];

          this._project.next(projects);

          return updatedProject;

        })
      ))
    )
    // return this.http.post(`${this.apiUrl}/project/update/${projectId}`, data)
  }


  deleteProject(id: any): Observable<any> {
    return this.projects$.pipe(
      take(1),
      switchMap(projects => this.http.delete(`${this.apiUrl}/project/delete/${id}`).pipe(
        map((isDeleted: boolean) => {

          // Find the index of the deleted project
          const index = projects.findIndex(item => item._id === id)

          // Delete the project
          projects.splice(index, 1);

          // Update the contacts
          this._projects.next(projects);

          // Return the deleted status
          return isDeleted;

        })
      ))
    )
  }


  getProjectById(pId): Observable<any> {

    if(!this._projects){
      return this.http.get(`${this.apiUrl}/project/details/${pId}`);
    }

    return this._projects.pipe(
      take(1),
      map((projects) => {

        // finding the project
        const project = projects.find(item => item._id === pId);

        // udpate the project
        this._project.next(project)

        /* return the selected project */
        return project

      }),
      switchMap((project) => {

        if (!project) {
          return throwError('Could not found project with id of ' + pId + '!');
        }

        return of(project);

      })
    )
  }


  getBuildingsOfProject(pid) {
    return this.http.get(`${this.apiUrl}/project/building/list/${pid}`);
  }

  getPorpertiesOfProject(data) {
    return this.http.post(`${this.apiUrl}/project/property/list`, {
      "project": data.project,
      "building": data.building,
      "floor": data.floor,
      "type": data.type
    });
  }


  addNewBuilding(data, editMode, bId?) {
    if (editMode) {
      return this.http.post(`${this.apiUrl}/project/building/update/${bId}`, data)
    } else {
      return this.http.post(`${this.apiUrl}/project/building/add`, data)
    }
  }

  addFlatOrPlot(data) {
    return this.http.post(`${this.apiUrl}/project/property/add`, data)
  }

  /* Deleting a building */
  deleteBuilding(bId) {
    return this.http.delete(`${this.apiUrl}/project/building/delete/${bId}`)
  }

  /* Deleting a  plot */
  deletePlot(pId) {
    return this.http.delete(`${this.apiUrl}/project/property/delete/${pId}`);
  }


  /* For changing published status  */
  changeStatus(projectId, type){
    return this.http.get(`${this.apiUrl}/project/change-status/${type}/${projectId}`);
  }


  getBanks() {
    return this.http.get(`${this.apiUrl}/bank/list`);
  }

  getAmenities() {
    return this.http.get(`${this.apiUrl}/amenities/list`);
  }
}
