import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { ProjectsService } from '../../projects/projects.service';
import { PropertyService } from '../property.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  constructor(private propertyService: PropertyService, private route: ActivatedRoute, private projectService: ProjectsService, private toast: ToastrService) { }

  property;
  imageUrl = environment.imageUrl;

  ngOnInit(): void {
    this.route.params.subscribe(
      prams => {
        this.propertyService.getPropertyById(prams.propertyId).subscribe(
          res => {
            this.property = res
            console.log({
              property: res
            })
          }
        )
      }
    )
  }

  changeStatus(propId) {
    this.property.published = !this.property.published;

    this.projectService.changeStatus(propId, 'Plot').subscribe(
      res => {
        if (res['status'] == 'success') {
          this.toast.success('Property published successfully');
        } else {
          this.toast.error(res['error'])
        }
      }
    )
  }


}
