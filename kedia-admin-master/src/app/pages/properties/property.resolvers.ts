import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { PropertyService } from './property.service';



@Injectable({
  providedIn: 'root'
})
export class  PropertiesResolver implements Resolve<any>{

  constructor(
    private propertyService: PropertyService
  ) { }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    return this.propertyService.getProperties({});
  }
}


@Injectable({
  providedIn: 'root'
})
export class PropertiesPropertyResolver implements Resolve<any>{

  constructor(
    private propertyService: PropertyService,
    private router: Router
  ) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    console.log({
      route: route.paramMap.get('propertyId')
    });

    return this.propertyService.getPropertyById(route.paramMap.get('propertyId'))
      .pipe(
        catchError((error) => {
          console.log({
            error
          })

          // Get the parent url
          // const parentUrl = state.url.split('/').slice(0, -1).join('/');

          // Navigate to there
          // this.router.navigateByUrl(parentUrl);

          // Throw an error
          return throwError(error);
        })
      )

  }
}

