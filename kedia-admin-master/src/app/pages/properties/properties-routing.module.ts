import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { ListComponent } from './list/list.component';
import { PropertiesComponent } from './properties.component';
import { PropertiesResolver, PropertiesPropertyResolver } from './property.resolvers';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: '',
    component: PropertiesComponent,
    children: [
      {
        path: 'list',
        component: ListComponent,
        resolve: {
          properties: PropertiesResolver
        }
      },
      {
        path: 'property-details/:propertyId',
        component: DetailsComponent,
        resolve: {
          property: PropertiesPropertyResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PropertiesRoutingModule { }
