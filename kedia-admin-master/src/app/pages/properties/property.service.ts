import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { data } from 'jquery';
import { BehaviorSubject, Observable, throwError, of } from 'rxjs';
import { tap, take, switchMap, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  private _property: BehaviorSubject<any>
  private _properties: BehaviorSubject<any[]>;

  apiUrl = environment.apiUrl


  public set propertyData(data) {
    localStorage.setItem('propertyData', JSON.stringify(data))
  }


  public get propertyData(): string {
    return JSON.parse(localStorage.getItem('propertydata'));
  }


  /**
   *
   * @param http http client
   */
  constructor(private http: HttpClient) {
    this._property = new BehaviorSubject(null);
    this._properties = new BehaviorSubject(null);
  }

  /* Getter for single property */
  public get property$(): Observable<any> {
    return this._property.asObservable();
  }

  /* Getter for properties */
  public get properties$(): Observable<any> {
    return this._properties.asObservable()
  }


  /* Api call for getting properties and setting them */
  getProperties(data): Observable<any> {

    console.log({
      data
    })

    this.propertyData = data;

    return this.http.post(`${this.apiUrl}/project/property/list`, {
      project: data.project,
      building: data.building,
      floor: data.floor,
      type: 'Plot'
    }).pipe(
      tap((properties) => {
        this._properties.next(properties['data']);

        console.log({
          properties
        })
      })
    );
  }

  /* Adding new property */
  addNewProperty(data) {

    return this.properties$.pipe(
      take(1),
      switchMap((properties) => this.http.post<any>(`${this.apiUrl}/project/property/add`, data).pipe(
        map((newProperty) => {

          // Update the contacts with the new contact
          this._properties.next([newProperty['data'], ...properties]);

          // Return the new contact
          return newProperty['data'];
        })
      ))
    );
  }


  deleteProperty(id: any): Observable<any> {
    return this.properties$.pipe(
      take(1),
      switchMap(properties => this.http.delete(`${this.apiUrl}/project/property/delete/${id}`).pipe(
        map((isDeleted: boolean) => {

          // Find the index of the deleted property
          const index = properties.findIndex(item => item._id === id)

          // Delete the property
          properties.splice(index, 1);

          // Update the contacts
          this._properties.next(properties);

          // Return the deleted status
          return isDeleted;

        })
      ))
    )
  }


  getPropertyById(pId): Observable<any> {

    if (!this._properties) {
      return this.http.get(`${this.apiUrl}/property/detail/${pId}`);
    }

    return this._properties.pipe(
      take(1),
      map((properties) => {

        // finding the property
        const property = properties.find(item => item._id === pId);

        // udpate the property
        this._property.next(property)

        /* return the selected property */
        return property

      }),
      switchMap((property) => {

        if (!property) {
          return throwError('Could not found property with id of ' + pId + '!');
        }

        return of(property);

      })
    )
  }

  /* getting the project list */
  getProjectList() {
    return this.http.get(`${this.apiUrl}/project/list`);
  }

  /* getting the buildings list */
  getBuildingList(projectId) {
    return this.http.get(`${this.apiUrl}/project/building/list/${projectId}`)
  }

}
