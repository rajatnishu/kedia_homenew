import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PropertyService } from '../property.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  properties$: Observable<any>;
  propertiesCount: number;

  projects = [];

  imageUrl = environment.imageUrl;

  /* Private */
  private _unsubscribeAll: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private propertyService: PropertyService,
    private router: Router,
    private toast: ToastrService
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();

    this.propertiesCount = 0;
  }

  viewList = true


  addPropertyForm;


  ngOnInit(): void {

    this.initForm();

    this.properties$ = this.propertyService.properties$;
    this.propertyService.properties$
      .pipe(takeUntil(this._unsubscribeAll)).subscribe(
        (properties: any[]) => {
          this.propertiesCount = properties?.length | 0
        }
      )

    this.getProjects();

  }

  search = {
    project: '',
    building: '',
    floor: '',
    type: '',
  }

  toggleListGridView() {
    this.viewList = !this.viewList
  }


  initForm(data?) {
    /* Add / Update projet Form */
    this.addPropertyForm = this.fb.group({

      type: [data?.type, Validators.required],
      project: [data?.project, Validators.required],

      description: [data?.description, Validators.required],
      areaSqft: [data?.areaSqft, Validators.required],
      serial: [data?.serial, Validators.required],
      bedrooms: [data?.bedrooms, Validators.required],
      bathrooms: [data?.bathrooms, Validators.required],
      livingRoom: [data?.livingRoom, Validators.required],
      balcony: [data?.balcony == true ? 'yes' : 'no', Validators.required],
      price: [data?.price, Validators.required],
      bookingPrice: [data?.bookingPrice, Validators.required],
      floor: [data?.floor, Validators.required],
      building: [data?.building, Validators.required],
      possessionStatus: [data?.possessionStatus, Validators.required],
      perSqFtPrice: [data?.perSqFtPrice, Validators.required],
    })
  }


  getProjects() {
    this.addPropertyForm.reset();
    this.propertyService.getProjectList().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.projects = res['data'];
        } else {
          this.projects[0] = {
            _id: '',
            name: "No projects found"
          }
        }
      }
    )
  }

  propertyImages = [];
  previewImages = [];
  uploadImages(files) {

    if (files.length > 0) {
      this.propertyImages = Array.from(files);

      this.propertyImages.forEach(image => {
        const reader = new FileReader();
        reader.readAsDataURL(image);

        reader.onload = event => {
          this.previewImages.push(reader.result);
        };
      });
    }
  }

  planImage;

  uploadPlanImage(file) {

    this.planImage = file[0]
    console.log({
      file,
      pi: this.planImage
    })
  }

  images360;
  upload360Images(files) {
    this.images360 = files[0]
  }

  /* For adding/updating a property */
  addProperty() {

    const formData = new FormData();

    /* getting data of form */
    let data = this.addPropertyForm.getRawValue();

    if (data.type == 'Flat' && (!data.serial || !data.floor || !data.building)) {
      this.toast.error("Please select flat serial, floor number and building.");
      return
    }

    for (var key in data) {
      formData.append(key, data[key]);
    }

    // formData.append('balcony', this.addPropertyForm.value.balcony === 'true' ? true : false);

    if (this.propertyImages.length > 0) {
      this.propertyImages.forEach((element, i) => {
        formData.append('images', element);
      });
    }

    formData.append('images360', this.images360)

    if (!this.editMode) {
      this.propertyService.addNewProperty(formData).subscribe(
        res => {
          if (res['status'] == 'success') {
            document.getElementById("closeModal").click();
            this.toast.success('Property added successfully.');
          } else {
            this.toast.success(res['error']);
          }
        }
      )
    } else {
      this.toast.success("Property updated successfully.");
      document.getElementById("closeModal").click();
      this.editMode = false

      // this.propertyService.updateProperty(formData).subscribe(
      //   res => {
      //     if (res['status'] == 'success') {
      //       document.getElementById("closeModal").click();
      //       this.toast.success('Property added successfully.');
      //     } else {
      //       this.toast.success(res['error']);
      //     }
      //   }
      // )
    }
  }


  propertyDetails(pId) {
    this.router.navigate(['/properties/property-details', pId]);
  }


  deleteProperty(pId) {
    if (confirm("Are you sure want to delete this property ?")) {
      this.propertyService.deleteProperty(pId).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.toast.success("Property deleted successfully.")
          } else {
            this.toast.error(`${res['error']}`)
          }
        }
      )
    }
  }

  /* for checking if it is edit mode or not */
  editMode = false;
  getPropertyForEdit(pId) {

    this.propertyService.getPropertyById(pId).subscribe(
      projData => {

        document.getElementById("addPropertyBtn").click();

        this.initForm(projData)

        this.editMode = true;

        this.previewImages = projData['images'];

      }
    )
  }


  buildings = [];
  propertyType(type) {

    if (type.value == 'Flat' && this.buildings.length == 0) {
      this.propertyService.getBuildingList(this.addPropertyForm.value.project).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.buildings = res['data']
          } else {
            this.buildings[0] = {
              name: "No buildings found."
            }
          }
        }
      )
    }
  }


  selectProject(projectId) {
    this.propertyService.getProperties({
      project: projectId,
      type: 'Plot'
    }).subscribe();
  }

}
