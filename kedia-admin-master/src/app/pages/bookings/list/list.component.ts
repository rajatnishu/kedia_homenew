import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { BookingService } from '../booking.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(private bookingService: BookingService, private toast: ToastrService) { }

  bookingsList
  imageUrl = environment.imageUrl;

  ngOnInit(): void {
    this.bookingService.getAllBookings().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.bookingsList = res['data'];
        } else {
          console.log({
            res
          })
        }
      }
    )
  }

  deleteBooking(bId, index) {
    if (confirm('Are you sure want to delete this booking?')) {
      this.bookingService.deleteBooking(bId).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.bookingsList.splice(index, 1);
            this.toast.success('Booking deleted successfully')
          } else {
            this.toast.error(res['error'])
          }
        }
      )
    }
  }

  activeBooking;
  setActiveBooking(booking) {
    this.activeBooking = booking
    console.log({
      booking
    })
  }
}
