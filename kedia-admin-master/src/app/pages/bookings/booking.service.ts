import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.apiUrl;

  getAllBookings() {
    return this.http.post(`${this.apiUrl}/booking/list`, {
      start: '', end: ''
    })
  }

  deleteBooking(bId){
    return this.http.delete(`${this.apiUrl}/booking/delete/${bId}`);
  }
}
