import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MidSectionImagesComponent } from './mid-section-images/mid-section-images.component';
import { TopSliderListComponent } from './top-slider-list/top-slider-list.component';

import { TopSliderComponent } from './top-slider.component';

const routes: Routes = [
  {
    path: '',
    component: TopSliderComponent,
    children: [
      {
        path: '',
        component: TopSliderListComponent
      },
      {
        path: 'mid-section-images',
        component: MidSectionImagesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TopSliderRoutingModule { }
