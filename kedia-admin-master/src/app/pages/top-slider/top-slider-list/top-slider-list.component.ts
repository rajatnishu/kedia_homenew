import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { TopSliderService } from '../top-slider.service';

@Component({
  selector: 'app-top-slider-list',
  templateUrl: './top-slider-list.component.html',
  styleUrls: ['./top-slider-list.component.css']
})
export class TopSliderListComponent implements OnInit {

  constructor(private topSliderService: TopSliderService, private fb: FormBuilder, private toast: ToastrService) { }

  imageUrl = environment.imageUrl;
  imagesList;

  /* for add/update image */
  editMode

  addImageFrom: FormGroup;

  ngOnInit(): void {
    this.initForm();
    this.topSliderService.getImages().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.imagesList = res['data'];
        } else {
          console.log({
            res
          })
        }
      }
    )
  }

  initForm(data?) {
    this.addImageFrom = this.fb.group({
      heading: [''],
      type: ['', Validators.required],
      image: ['', Validators.required],
      projectLink: ['', Validators.required]
    })
  }

  imagePreview
  imageFile;
  height
  width
  uploadImage(files) {
    this.imageFile = files[0];

    console.log({
      file: files[0]
    })

    const reader = new FileReader()
    reader.readAsDataURL(files[0])

    let This = this;
    reader.onload = event => {
      this.imagePreview = reader.result;

      var image = new Image();
      image.src = event.target.result.toString();
      let self = image;
      image.onload = function () {
        var height = self.height;
        var width = self.width;

        console.log({
          height,
          width
        });

        if (width < 570 || height < 663) {
          This.imageFile = null;
          This.imagePreview = null;
          alert('Please select image with width 570 and height 663 or above');
        }
        document.getElementById("width").innerHTML = width.toString();
        document.getElementById("height").innerHTML = height.toString();
      }
    };
  }

  deleteImage(imageId, index) {
    if (confirm('Are you sure want to perform this action ?')) {
      this.topSliderService.deleteImage(imageId).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.imagesList.splice(index, 1)
            this.toast.success('Bank deleted successfully');
          } else {
            console.log({
              res
            })
          }
        }
      )
    }
  }

  submitImage() {
    const formData = new FormData();

    if (!this.imageFile) {
      this.toast.error('Valid image is required');
      return
    }

    formData.append('type', this.addImageFrom.value.type);
    formData.append('heading', this.addImageFrom.value.heading);
    formData.append('image', this.imageFile);
    formData.append('projectLink', this.addImageFrom.value.projectLink);

    if (this.addImageFrom.value.type == 'mid' && !this.addImageFrom.value.heading) {
      this.toast.error('Heading is required for mid type images.');
      return;
    }

    this.topSliderService.addImage(formData).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.toast.success('Image added successfully.');
          this.imagesList.push(res['data']);
          document.getElementById('closeModal').click();
        } else {
          console.log({
            res
          })
        }
      }
    )
  }

}
