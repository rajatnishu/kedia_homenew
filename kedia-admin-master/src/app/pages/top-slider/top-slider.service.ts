import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TopSliderService {

  constructor(private http: HttpClient) {
  }

  apiUrl = environment.apiUrl;

  getImages() {
    return this.http.get(`${this.apiUrl}/images/list`);
  }

  deleteImage(imgId) {
    return this.http.delete(`${this.apiUrl}/images/delete/${imgId}`)
  }

  addImage(data) {
    return this.http.post(`${this.apiUrl}/images/add`, data);
  }

  update(data, imgId) { }
}
