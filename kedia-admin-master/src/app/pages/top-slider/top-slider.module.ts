import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopSliderRoutingModule } from './top-slider-routing.module';
import { TopSliderComponent } from './top-slider.component';
import { TopSliderListComponent } from './top-slider-list/top-slider-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MidSectionImagesComponent } from './mid-section-images/mid-section-images.component';


@NgModule({
  declarations: [TopSliderComponent, TopSliderListComponent, MidSectionImagesComponent],
  imports: [
    CommonModule,
    TopSliderRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class TopSliderModule { }
