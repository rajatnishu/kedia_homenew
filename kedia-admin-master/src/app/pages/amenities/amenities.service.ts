import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AmenitiesService {

  constructor(private http: HttpClient) { }

  apiurl = environment.apiUrl;

  getAmenities() {
    return this.http.get(`${this.apiurl}/amenities/list`);
  }

  addAmenities(data) {
    return this.http.post(`${this.apiurl}/amenities/add`, data);
  }

  deleteAmenities(bId) {
    return this.http.delete(`${this.apiurl}/amenities/delete/${bId}`);
  }

}
