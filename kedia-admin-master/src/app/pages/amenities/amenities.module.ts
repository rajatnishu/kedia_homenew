import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AmenitiesRoutingModule } from './amenities-routing.module';
import { AmenitiesComponent } from './amenities.component';
import { AmenitiesListComponent } from './amenities-list/amenities-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [AmenitiesComponent, AmenitiesListComponent],
  imports: [
    CommonModule,
    AmenitiesRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AmenitiesModule { }
