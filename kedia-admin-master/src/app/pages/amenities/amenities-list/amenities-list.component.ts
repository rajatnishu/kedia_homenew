import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { AmenitiesService } from '../../amenities/amenities.service';

@Component({
  selector: 'app-amenities-list',
  templateUrl: './amenities-list.component.html',
  styleUrls: ['./amenities-list.component.css']
})
export class AmenitiesListComponent implements OnInit {

  constructor(private fb: FormBuilder, private amenitiesService: AmenitiesService, private toast: ToastrService) { }

  imageUrl = environment.imageUrl;
  addAmenityFrom: FormGroup;
  editMode = false;

  amenityList;

  ngOnInit(): void {
    this.initForm()

    this.amenitiesService.getAmenities().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.amenityList = res['data']
          console.log({
            res
          }, {
            depth: null,
            color: 'red'
          })
        } else {
          console.log({
            res
          }, {
            depth: null,
            color: 'red'
          })
        }
      }
    )
  }

  initForm(data?) {
    this.addAmenityFrom = this.fb.group({
      name: [data?.name, Validators.required],
      logo: [data?.logo, Validators.required]
    })
  }

  amenityLogo;
  amenityLogoPreview;
  uploadLogo(files) {
    this.amenityLogo = files[0];

    console.log({
      f: files[0]
    })

    const reader = new FileReader()
    reader.readAsDataURL(files[0])

    reader.onload = event => {
      this.amenityLogoPreview = reader.result;
    };
  }


  submitAmenity() {
    const formData = new FormData();
    formData.append('name', this.addAmenityFrom.value.name)
    formData.append('logo', this.amenityLogo)
    formData.append('loanPercent', this.addAmenityFrom.value.loanPercent)

    this.amenitiesService.addAmenities(formData).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.toast.success('Amenity added successfully');
          document.getElementById('closeModal').click();
          this.amenityList.push(res['data']);
          this.initForm();
        } else {
          this.toast.error(res['error'])
        }
      }
    )
  }

  deleteAmenity(ameId, index) {
    if (confirm('Are you sure want to perform this action ?')) {
      this.amenitiesService.deleteAmenities(ameId).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.amenityList.splice(index, 1)
            this.toast.success('Amenity deleted successfully');
          } else {
            console.log({
              res
            })
          }
        }
      )
    }
  }

}
