import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AmenitiesListComponent } from './amenities-list/amenities-list.component';

import { AmenitiesComponent } from './amenities.component';

const routes: Routes = [
  {
    path: '',
    component: AmenitiesComponent,
    children: [
      {
        path: '',
        component: AmenitiesListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmenitiesRoutingModule { }
