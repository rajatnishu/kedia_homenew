import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  constructor(private authService: AuthService, private toast: ToastrService) { }

  userList;
  imageUrl = environment?.imageUrl;

  ngOnInit(): void {
    this.authService.getAllUsers().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.userList = res['data']
        } else {
          console.log({
            res
          })
        }
      }
    )
  }

  deleteUser(userId, i) {
    if (confirm('Are you sure want to delete this user ?')) {
      this.authService.deleteUser(userId).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.toast.success('User deleted successfully.')
            this.userList.splice(i, 1)
          } else {
            this.toast.error(res['error'])
            console.log({
              res
            })
          }
        }
      )
    }
  }

}
