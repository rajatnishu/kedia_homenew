import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.apiUrl;

  public get accessToken(): string {
    return JSON.parse(localStorage.getItem('token'));
  }


  public set accessToken(token) {
    localStorage.setItem('token', JSON.stringify(token));
  }

  getAllUsers(){
    return this.http.get(`${this.apiUrl}/user/users-list`);
  }

  deleteUser(userId){
    return this.http.delete(`${this.apiUrl}/user/delete-user/${userId}`)
  }

  signOut() {

  }
}
