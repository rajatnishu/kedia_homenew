import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BankDetailsRoutingModule } from './bank-details-routing.module';
import { BankDetailsComponent } from './bank-details.component';
import { BankListComponent } from './bank-list/bank-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [BankDetailsComponent, BankListComponent],
  imports: [
    CommonModule,
    BankDetailsRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class BankDetailsModule { }
