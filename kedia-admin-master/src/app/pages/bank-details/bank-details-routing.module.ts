import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BankDetailsComponent } from './bank-details.component';
import { BankListComponent } from './bank-list/bank-list.component';

const routes: Routes = [
  {
    path: '',
    component: BankDetailsComponent,
    children: [
      {
        path: '',
        component: BankListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BankDetailsRoutingModule { }
