import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { BankService } from '../bank.service';

@Component({
  selector: 'app-bank-list',
  templateUrl: './bank-list.component.html',
  styleUrls: ['./bank-list.component.css']
})
export class BankListComponent implements OnInit {

  constructor(private fb: FormBuilder, private bankService: BankService, private toast: ToastrService) { }

  imageUrl = environment.imageUrl;
  addBankFrom: FormGroup;
  editMode = false;

  banksList;

  ngOnInit(): void {
    this.initForm()

    this.bankService.getBanks().subscribe(
      res => {
        if (res['status'] == 'success') {
          this.banksList = res['data']
          console.log({
            res
          }, {
            depth: null,
            color: 'red'
          })
        } else {
          console.log({
            res
          }, {
            depth: null,
            color: 'red'
          })
        }
      }
    )
  }

  initForm(data?) {
    this.addBankFrom = this.fb.group({
      name: [data?.name, Validators.required],
      logo: [data?.logo, Validators.required],
      loanPercent: [data?.loanPercent, Validators.required]
    })
  }

  bankLogo;
  bankLogoPreview;
  uploadLogo(files) {
    this.bankLogo = files[0];

    const reader = new FileReader()
    reader.readAsDataURL(files[0])

    reader.onload = event => {
      this.bankLogoPreview = reader.result;
    };
  }


  submitBank() {
    const formData = new FormData();
    formData.append('name', this.addBankFrom.value.name)
    formData.append('logo', this.bankLogo)
    formData.append('loanPercent', this.addBankFrom.value.loanPercent)

    this.bankService.addBank(formData).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.toast.success('Bank added successfully');
          document.getElementById('closeModal').click();
          this.banksList.push(res['data']);
          this.initForm();
        } else {
          this.toast.error(res['error'])
        }
      }
    )
  }

  deleteBank(bankId, index) {
    if (confirm('Are you sure want to perform this action ?')) {
      this.bankService.deleteBank(bankId).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.banksList.splice(index, 1)
            this.toast.success('Bank deleted successfully');
          } else {
            console.log({
              res
            })
          }
        }
      )
    }
  }

}
