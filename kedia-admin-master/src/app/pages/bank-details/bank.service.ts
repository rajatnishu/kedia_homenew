import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BankService {

  constructor(private http: HttpClient) { }

  apiurl = environment.apiUrl;

  getBanks() {
    return this.http.get(`${this.apiurl}/bank/list`);
  }

  addBank(data) {
    return this.http.post(`${this.apiurl}/bank/add`, data);
  }

  deleteBank(bId){
    return this.http.delete(`${this.apiurl}/bank/delete/${bId}`);
  }

}
