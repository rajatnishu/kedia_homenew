import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BuildingsService } from './buildings.service';

@Injectable({
  providedIn: 'root'
})
export class BuildingDetailsResolver implements Resolve<any>{

  constructor(private buildingService: BuildingsService, private router: Router) { }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

    return this.buildingService.getBuildingDetails(route.paramMap.get('buildingId'))
      .pipe(
        catchError((error) => {
          console.log({
            error
          })

          // Throw an error
          return throwError(error);
        })
      )

  }

}
