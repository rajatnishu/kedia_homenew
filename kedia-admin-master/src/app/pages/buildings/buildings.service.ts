import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BuildingsService {

  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }


  getBuildingDetails(id) {
    return this.http.get(`${this.apiUrl}/project/building/detail/${id}`);

  }

  addFlat(data) {
    return this.http.post(`${this.apiUrl}/project/property/add`, data)
  }

  updateFlat(data, pId) {
    return this.http.post(`${this.apiUrl}/project/property/update/${pId}`, data)
  }

  deleteFlat(pId) {
    return this.http.delete(`${this.apiUrl}/property/delete/${pId}`)
  }


  /* getting flat details */
  getFlatDetails(fId) {
    return this.http.get(`${this.apiUrl}/project/property/detail/${fId}`);
  }

}
