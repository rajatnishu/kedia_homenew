import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { ProjectsService } from '../../projects/projects.service';
import { BuildingsService } from '../buildings.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private buildingService: BuildingsService, private toast: ToastrService, private fb: FormBuilder, private projectService: ProjectsService, private route: ActivatedRoute) { }
  imageUrl = environment.imageUrl;

  buildingDetails;
  buildId;

  addFlatForm: FormGroup;

  ngOnInit(): void {
    this.initForm()
    this.activatedRoute.data.subscribe(
      data => {
        console.log({
          data
        })
        if (data.buildingDetails.status == 'success') {
          this.buildingDetails = data.buildingDetails.data
        }
      }
    )

    this.route.params.subscribe(
      res => {
        this.buildId = res.buildingId
      }
    )
  }

  getBuildingDetails() {
    this.buildingService.getBuildingDetails(this.buildId).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.buildingDetails = res['data']
        } else {
          console.log({
            res
          })
        }
      }
    )
  }

  initForm() {
    this.addFlatForm = this.fb.group({
      type: ['Flat'],
      description: [''],
      areaSqft: ['', Validators.required],
      serial: ['', Validators.required],
      bedrooms: ['', Validators.required],
      bathrooms: ['', Validators.required],
      livingRoom: ['', Validators.required],
      balcony: ['', Validators.required],
      price: ['', Validators.required],
      bookingPrice: ['', Validators.required],
      floor: ['', Validators.required],
      possessionStatus: ['', Validators.required],
      perSqFtPrice: ['', Validators.required]
    })
  }

  /* For adding flat */
  /*  flatDetails = {
     type: 'Flat',
     description: '',
     areaSqft: '',
     serial: '',
     bedrooms: '',
     bathrooms: '',
     livingRoom: '',
     balcony: '',
     price: 0,
     bookingPrice: 0,
     floor: '',
   } */

  planImage;
  planImagePreview
  images;
  imagesPreview = []

  uploadPlanImage(file) {

    this.planImage = file[0]

    const reader = new FileReader()
    reader.readAsDataURL(file[0])

    reader.onload = event => {
      this.planImagePreview = reader.result
    }
  }

  uploadImages(files) {

    this.images = Array.from(files)

    this.images.forEach(image => {
      const reader = new FileReader();
      reader.readAsDataURL(image);

      reader.onload = event => {
        this.imagesPreview.push(reader.result);
      };
    });
  }

  images360;
  upload360Images(files) {
    this.images360 = files[0]
  }

  addFlat() {

    const formData = new FormData();

    formData.append('type', this.addFlatForm.value.type);
    formData.append('description', this.addFlatForm.value.description);
    formData.append('areaSqft', this.addFlatForm.value.areaSqft);
    formData.append('serial', this.addFlatForm.value.serial);
    formData.append('bedrooms', this.addFlatForm.value.bedrooms);
    formData.append('bathrooms', this.addFlatForm.value.bathrooms);
    formData.append('livingRoom', this.addFlatForm.value.livingRoom);
    formData.append('balcony', this.addFlatForm.value.balcony);
    formData.append('price', `${this.addFlatForm.value.price}`);
    formData.append('bookingPrice', `${this.addFlatForm.value.bookingPrice}`);
    formData.append('floor', this.addFlatForm.value.floor);
    formData.append('building', this.buildingDetails._id);
    formData.append('project', this.buildingDetails.project._id);
    formData.append('possessionStatus', this.addFlatForm.value.possessionStatus);
    formData.append('perSqFtPrice', this.addFlatForm.value.perSqFtPrice);

    if (this.planImage) {
      formData.append('planImage', this.planImage)
    }

    if (this.images.length > 0) {
      this.images.forEach((element, i) => {
        formData.append('images', element);
      });
    }

    if (this.images360) {
      formData.append('images360', this.images360)
    }

    console.log({
      a: formData
    })

    this.buildingService.addFlat(formData).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.buildingDetails.property.push(this.addFlatForm.value)
          this.toast.success('Flat added to property successfully');
          document.getElementById("details-page").click();
        } else {
          if (res['error']) {
            this.toast.error(res['error'])
          } else {
            this.toast.error('Only images with PNG, JPG, GIF, and JPEG extentions are allowed!')
          }
        }
      }
    )
  }

  deleteFlat(fId, index) {
    if (confirm('Are you sure want to delete this flat?')) {
      this.buildingService.deleteFlat(fId).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.toast.success("Flat deleted successfully");
            this.buildingDetails.flats.splice(index, 1);
          } else {
            this.toast.error(res['error'])
          }
        }
      )
    }
  }

  /* selecting flat to make duplicate */
  selectedFlat;
  setFlatForDuplicate(flat) {
    this.selectedFlat = flat;
    console.log({
      flat
    })
  }

  /* creating Array according to input for iteration */
  numOfDuplicate: number = 1;
  duplicateArray;
  setDuplicate(value) {
    this.numOfDuplicate = value;
    this.duplicateArray = Array(+this.numOfDuplicate);
  }


  /* Setting duplicate data to submit*/
  data = {
    serial: [],
    floor: []
  }


  submitDuplicate() {
    console.log({
      data: this.data
    })

    if (this.data.floor.length != +this.numOfDuplicate || this.data.serial.length != +this.numOfDuplicate) {
      this.toast.error('Please fill all fields')
      return
    }


    /*  creating form data to submit */
    this.data.floor.forEach((element, index) => {
      debugger
      const formData = new FormData();

      formData.append('type', 'Flat');
      formData.append('description', this.selectedFlat.description)
      formData.append('areaSqft', this.selectedFlat.areaSqft)
      formData.append('planImage', this.selectedFlat.planImage)

      // let images = []
      // this.selectedFlat.images.forEach(image => {
      //   images.
      // });

      formData.append('images', this.selectedFlat.images)
      formData.append('bedrooms', this.selectedFlat.bedrooms)
      formData.append('bathrooms', this.selectedFlat.bathrooms)
      formData.append('livingRoom', this.selectedFlat.livingRoom)
      formData.append('balcony', this.selectedFlat.balcony)
      formData.append('facing', this.selectedFlat.facing)

      formData.append('price', this.selectedFlat.price)
      formData.append('bookingPrice', this.selectedFlat.bookingPrice)

      formData.append('building', this.selectedFlat.building)
      formData.append('project', this.selectedFlat.project)

      formData.append('possessionStatus', this.selectedFlat.project)
      formData.append('perSqFtPrice', this.selectedFlat.project)

      formData.append('serial', this.data.serial[index])
      formData.append('floor', this.data.floor[index])


      this.buildingService.addFlat(formData).subscribe(
        res => {
          if (res['status'] == 'success') {
            this.buildingDetails.property.push(res['data']);

            document.getElementById("close").click();
          } else {
            console.log({
              res
            })
          }
        }
      )

    });


  }

  changeStatus(buildingId, flatIndex = null) {

    let type = 'building';

    if (flatIndex > -1) {
      type = 'property';
      this.buildingDetails.property[flatIndex].published = !this.buildingDetails.property[flatIndex].published;
    } else {
      this.buildingDetails.published = !this.buildingDetails.published;
    }


    this.projectService.changeStatus(buildingId, type).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.toast.success(`${type == 'building' ? 'Building' : 'Flat'}\'s status changed successfully`);

        } else {
          console.log({
            res
          })
          this.toast.error('Something went wrong')
        }
      }
    )
  }
}
