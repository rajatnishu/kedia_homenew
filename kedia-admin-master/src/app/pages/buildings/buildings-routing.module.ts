import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildingsComponent } from './buildings.component';
import { BuildingDetailsResolver } from './buildings.resolvers';
import { DetailsComponent } from './details/details.component';
import { FlatsUpdateComponent } from './flats-update/flats-update.component';

const routes: Routes = [
  // {
  //   path: '', redirectTo: 'details/:buildingId', pathMatch: 'full'
  // },
  {
    path: '',
    component: BuildingsComponent,
    children: [
      {
        path: 'details/:buildingId',
        resolve: {
          buildingDetails: BuildingDetailsResolver
        },
        component: DetailsComponent
      },
      {
        path: 'update-flat/:id',
        component: FlatsUpdateComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuildingsRoutingModule { }
