import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { BuildingsService } from '../buildings.service';

@Component({
  selector: 'app-flats-update',
  templateUrl: './flats-update.component.html',
  styleUrls: ['./flats-update.component.css']
})
export class FlatsUpdateComponent implements OnInit {

  constructor(private route: ActivatedRoute, private buildingService: BuildingsService, private fb: FormBuilder, private toast: ToastrService, private router: Router) { }

  flatDetails;
  flatUpdateForm: FormGroup;

  flatImagesCurrent = [];
  flatPlanImageCurrent;
  images360Current;

  imageUrl = environment.imageUrl;

  ngOnInit(): void {
    this.initForm();

    this.route.params.subscribe(
      flatId => {
        this.buildingService.getFlatDetails(flatId.id).subscribe(
          res => {
            if (res['status'] == 'success') {
              this.flatDetails = res['data'];
              this.flatUpdateForm.patchValue(res['data'])
              this.flatUpdateForm.patchValue({
                project: res['data'].project._id,
                building: res['data'].building._id,
                balcony: res['data'].balcony ? 'yes' : 'no'
              });
              this.flatImagesCurrent = res['data'].images;
              this.flatPlanImageCurrent = res['data'].planImage
              this.images360Current = res['data'].images360
            } else {
              console.log({
                res
              })
            }
          }
        )
      }
    )

  }


  initForm() {
    this.flatUpdateForm = this.fb.group({
      type: ['Flat'],
      description: [''],
      areaSqft: ['', Validators.required],
      serial: ['', Validators.required],
      planImage: ['', Validators.required],
      images: ['', Validators.required],
      bedrooms: ['', Validators.required],
      bathrooms: ['', Validators.required],
      livingRoom: ['', Validators.required],
      balcony: ['', Validators.required],
      floor: ['', Validators.required],
      price: ['', Validators.required],
      bookingPrice: ['', Validators.required],
      possessionStatus: ['', Validators.required],
      perSqFtPrice: ['', Validators.required],
      building: [''],
      project: ['']
    });

  }


  updateFlat() {

    const formData = new FormData();

    formData.append('type', this.flatUpdateForm.value.type);
    formData.append('description', this.flatUpdateForm.value.description);
    formData.append('areaSqft', this.flatUpdateForm.value.areaSqft);
    formData.append('serial', this.flatUpdateForm.value.serial);
    formData.append('planImage', this.planImage);

    formData.append('bedrooms', this.flatUpdateForm.value.bedrooms);
    formData.append('bathrooms', this.flatUpdateForm.value.bathrooms);
    formData.append('livingRoom', this.flatUpdateForm.value.livingRoom);
    formData.append('balcony', this.flatUpdateForm.value.balcony == 'yes' ? 'true' : 'false');
    formData.append('floor', this.flatUpdateForm.value.floor);
    formData.append('price', this.flatUpdateForm.value.price);
    formData.append('bookingPrice', this.flatUpdateForm.value.bookingPrice);
    formData.append('building', this.flatUpdateForm.value.building);
    formData.append('project', this.flatUpdateForm.value.project);
    formData.append('possessionStatus', this.flatUpdateForm.value.possessionStatus);
    formData.append('perSqFtPrice', this.flatUpdateForm.value.perSqFtPrice);
    formData.append('facing', 'East');

    this.images.forEach(img => {
      formData.append('images', img);
    })
    debugger
    this.buildingService.updateFlat(formData, this.flatDetails._id).subscribe(
      res => {
        if (res['status'] == 'success') {
          this.toast.success('Flat updated successfully');
          this.router.navigate(['./'])
        } else {
          this.toast.error(res['error'])
          console.log({
            errror: res
          })
        }
      }
    )
  }

  planImage;
  planImagePreview;
  images = [];
  imagesPreview = [];
  images360;
  uploadImage(files, type) {

    if (type == 'plan') {
      this.planImage = files[0];

      const reader = new FileReader();
      reader.readAsDataURL(files[0])

      reader.onload = event => {
        this.planImagePreview = reader.result;
      }
    } else if (type == 'images') {
      for (const img of files) {
        this.images.push(img)

        const reader = new FileReader();
        reader.readAsDataURL(img)

        reader.onload = event => {
          this.imagesPreview.push(reader.result);
        }
      }
    } else {
      this.images360 = files[0]
    }

  }

}
