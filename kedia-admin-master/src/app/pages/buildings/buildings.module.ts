import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuildingsRoutingModule } from './buildings-routing.module';
import { BuildingsComponent } from './buildings.component';
import { DetailsComponent } from './details/details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { FlatsUpdateComponent } from './flats-update/flats-update.component';


@NgModule({
  declarations: [BuildingsComponent, DetailsComponent, FlatsUpdateComponent],
  imports: [
    CommonModule,
    BuildingsRoutingModule,
    FormsModule,
    DataTablesModule,
    ReactiveFormsModule
  ]
})
export class BuildingsModule { }
