const express = require('express')
const router = express.Router()
const path = require('path')
const Image = require('../models/Image')
const utils = require('../utils')
const multer = require('multer')


// SET STORAGE
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/top-bottom-images')
    },
    filename: function (req, file, cb) {
        let a = file.originalname.split('.')
        cb(null, `${file.fieldname}-${Date.now()}.${a[a.length - 1]}`)
    }
})

const upload = multer({
    storage: storage,
    fileFilter: function (req, file, callback) {
        var ext = path.extname(file.originalname);
        if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
            return callback(utils.createError("Only images are allowed!"))
        }
        callback(null, true)
    },
    limits: {
        fileSize: 1024 * 1024 * 10
    }
})

router.get('/list', (request, response) => {
    console.log(request.file)
    try {
        Image.find({ deleted: false }, { deleted: 0, __v: 0, createdOn: 0 }, (error, images) => {
            if (error) {
                response.send(utils.createError(error))
            } else if (!images) {
                response.send(utils.createError('Images not found'))
            } else {
                response.send(utils.createResult(error, images));
            }
        })
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.get('/get/:id', (request, response) => {
    try {
        const { id } = request.params;
        if (id) {
            Image.findOne({ _id: id }, { deleted: 0, __v: 0, createdOn: 0 }, (error, image) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!image) {
                    response.send(utils.createError('image not found'))
                } else {
                    response.send(utils.createResult(error, image));
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/add', upload.single('image'), (request, response) => {
    console.log(request.file)
    try {
        const {
            type,
            heading,
            projectLink
        } = request.body;
        if (type) {
            let image = new Image();

            image.type = type
            image.heading = heading
            image.projectLink = projectLink

            if (request.file) image.image = `uploads/top-bottom-images/${request.file.filename}`;

            image.save((error, image) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!image) {
                    response.send(utils.createError('Image not found'))
                } else {
                    response.send(utils.createResult(error, image));
                }
            })
        } else {
            response.send(utils.createError('name is required'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/update/:id', upload.single('image'), (request, response) => {
    console.log(request.file)
    try {
        const { id } = request.params;
        const {
            type,
            status,
            heading,
            projectLink
        } = request.body
        if (id) {
            Image.findOne({ _id: id }, { deleted: 0, __v: 0, createdOn: 0 }, (error, image) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!image) {
                    response.send(utils.createError('Image not found'))
                } else {

                    if (type) image.type = type
                    if (status) image.status = status
                    if (heading) image.heading = heading
                    if(projectLink) image.projectLink = projectLink
                    if (request.file) image.image = `uploads/top-bottom-images/${request.file.filename}`;

                    image.save((error, image) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!image) {
                            response.send(utils.createError('Image not found'))
                        } else {
                            response.send(utils.createResult(error, image));
                        }
                    })
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.delete('/delete/:id', (request, response) => {
    console.log(request.file)
    try {
        const { id } = request.params;
        if (id) {
            Image.findOne({ _id: id }, { deleted: 0, __v: 0, createdOn: 0 }, (error, image) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!image) {
                    response.send(utils.createError('image not found'))
                } else {

                    image.deleted = true;

                    image.save((error, image) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!image) {
                            response.send(utils.createError('Image not found'))
                        } else {
                            response.send(utils.createSuccess(image));
                        }
                    })
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/change-status/:id', (request, response) => {
    try {
        const { id } = request.params;
        const { status } = request.body;
        if (id) {
            Image.findOne({ _id: id }, { deleted: 0, __v: 0, createdOn: 0 }, (error, image) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!image) {
                    response.send(utils.createError('image not found'))
                } else {

                    image.status = status ? 'active' : 'inactive'

                    image.save((error, image) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!image) {
                            response.send(utils.createError('Image not found'))
                        } else {
                            response.send(utils.createSuccess(image));
                        }
                    })
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

module.exports = router;