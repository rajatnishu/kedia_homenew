const express = require('express')
const router = express.Router()
const path = require('path')
const User = require('../models/User')
const utils = require('../utils')
const Joi = require('@hapi/joi');
const cryptoJs = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../config/constants')
const mailer = require('./mailer')
const aws = require('aws-sdk');
const multer = require('multer')
const multerS3 = require('multer-s3');

// SET STORAGE
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    console.log('destination', file);
    try {
      cb(null, 'public/uploads/users')
    } catch (e) {
      cb(e)
    }
  },
  filename: function (req, file, cb) {
    console.log('filename', file);
    try {
      let a = file.originalname.split('.')
      cb(null, `${Date.now()}.${a[a.length - 1]}`)
    } catch (e) {
      cb(e)
    }


    //cb(null, file.fieldname + '-' + Date.now())
  }
})

const upload = multer({
  storage: storage,
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
      return callback(utils.createError("Only images with PNG, JPG, GIF, and JPEG extentions are allowed!"))
    }
    console.log("fileFilter")
    callback(null, true)
  },
  limits: {
    fileSize: 1024 * 1024 * 5 ,
    fieldSize: 1024 * 1024 * 5 ,
  }
})

router.get('/checkEmail/:email', function (request, response) {
  const { email } = request.params;
  if (!utils.validateEmail(email)) {
    return response.send(utils.createError("Invalid email"));
  }
  User.findOne({ email }, (error, user) => {
    if (error) {
      response.send(utils.createError('Database error'));
    } else if (user) {
      response.send(utils.createError('This email is already used'));
    } else {

      response.send(utils.createResult(error, "This email is available"))
    }
  })

})

router.post('/register', upload.single('aadharCard'), function (request, response) {
  const { fullName, email, password, phone, role, gender, latitude, longitude, device_token } = request.body

  const JoiSchema = Joi.object({
    fullName: Joi.string().required(),
    password: Joi.string().min(6).max(50).required(),
    phone: Joi.string().min(10).max(10).required(),
    email: Joi.string().email().min(5).max(50).required(),
    gender: Joi.string().valid('male')
      .valid('female').valid('other').optional(),
  }).options({ abortEarly: false });
  const { error } = JoiSchema.validate({ fullName, email, password, phone, gender })
  if (error) {
    return response.send(utils.createError(error));
  }

  User.findOne({ phone: phone }, (error, user) => {
    if (error) {
      response.send(utils.createError('Database error'));
    } else if (user) {
      response.send(utils.createError('This phone number/email is already registered with us'));
    } else {
      const user = new User()
      user.fullName = fullName
      user.email = email || ""
      user.password = cryptoJs.SHA256(password)
      user.phone = phone
      user.role = role
      user.gender = gender || ""
      user.device_token = device_token

      if (latitude && longitude)
        user.location = {
          type: 'Point',
          coordinates: [longitude, latitude]
        }

      if (request.file) {
        user.aadharCard = `uploads/users/${request.file.filename}`;
      }

      let token;
      if (user && user._id) {
        token = jwt.sign({
          id: user._id,
          email: `${user.email}`,
          role: user.type
        }, config.secret);
        response.header('X-Auth-Token', token);
      }
      user.access_token = token;
      user.save((error, result) => {
        console.log(error, result);
        if (error) return response.send(utils.createResult("Database error"))

        let message = `<h3>Congratulations ${fullName || 'User'}!</h3><p>Your account has been created on Kedia Homes.</p><br/><p>Team Kedia Home</p>`
        mailer.sendEmail(user.email, "Your Kedia Homes Account", message, (emailError, data) => {
          console.log("Email", emailError, data)
          response.send(utils.createResult(error, user.safeUser()))
        })
      })
    }
  })

})

router.post('/login', function (request, response) {
  debugger
  const { email, phone, password, device_token } = request.body

  User.findOne({ $or: [{ email }, { phone }] }, (error, user) => {
    console.log("user find()", error, user)
    if (error) {
      response.send(utils.createError('Database error'));
    } else if (!user) {
      response.send(utils.createError('invalid credentials!'))
    } else {
      const userPassword = cryptoJs.SHA256(password)
      if (userPassword == user.password) {
        user.device_token = device_token
        const token = jwt.sign({
          id: user._id,
          email: `${user.email}`,
          role: user.type
        }, config.secret);

        response.header('X-Auth-Token', token);
        user.access_token = token;
        user.save((error, result) => {
          console.log("user save", error, result)
          if (error) return response.send(utils.createError("Database error"))

          response.send(utils.createSuccess(result.safeUser()))
        })
      } else {
        response.send(utils.createError('invalid credentials!'))
      }
    }
  })
})

router.post('/verify-phone', (request, response) => {
  const { phone } = request.body
  User.findOne({ _id: request.userId }, (error, user) => {
    if (error) {
      response.send(utils.createError('Database error'));
    } else if (!user) {
      response.send(utils.createError('user not found'));
    } else {

      if (phone) user.phone = phone;
      else return response.send(utils.createError('Phone is required!'));

      const otp = utils.randomNumber()
      user.otp = cryptoJs.SHA256(otp);

      user.save((error, result) => {
        if (error) {
          console.log(error)
          return response.send(utils.createError('Database error'));
        }
        let body = `Hello ${user.firstName}!, Verification code to verify your phone number is: ${otp}`;
        try {
          let contact = result.isdCode + result.phone;
          mailer.sendSMS(contact, body, (message) => {
            console.log(message)
            response.send(utils.createSuccess(message))
          });
        } catch (ex) {
          console.log("endpoint catch", ex)
          response.send(utils.createError({ sid: "OTP SMS failed.".sid, body }))
        }
      })
    }
  })
})

router.post('/verify-phone-otp', (request, response) => {
  const { otp } = request.body
  User.findOne({ _id: request.userId }, (error, user) => {
    if (error) {
      response.send(utils.createError('Database error'));
    } else if (!user) {
      response.send(utils.createError('user not found'));
    } else {

      const userOtp = cryptoJs.SHA256(otp)
      if (userOtp == user.otp) {
        user.otp = ""
        user.phoneVerified = true;
        user.save((error, user) => {
          if (error) {
            console.log(error)
            return response.send(utils.createError('Database error'));
          }
          response.send(utils.createSuccess(user.safeUser()))
        })
      }
      else {
        return response.send(utils.createError('OTP verification failed!'));
      }

    }
  })
})

router.post('/reset-my-password', (request, response) => {
  const { password } = request.body
  if (!password || password.length < 6) {
    console.log("password error", password)
    return response.send(utils.createError("Paasword should atleast be 6 character long"));
  } else
    User.findOne({ _id: request.userId }, (error, user) => {
      if (error) {
        console.log("error find", error)
        response.send(utils.createError('Database error'));
      } else if (!user) {
        response.send(utils.createError('user not found'));
      } else {
        user.password = cryptoJs.SHA256(password)
        user.save((error, result) => {
          if (error) {
            console.log("error save", error)
            response.send(utils.createError('Database error'));
          } else {
            response.send(utils.createResult(error, result))
          }
        })
      }
    })
})

router.post('/forgot-password', (request, response) => {
  const { email, phone } = request.body


  User.findOne({ $or: [{ email }, { phone }] }, (error, user) => {
    if (error) {
      console.log("error find", error)
      response.send(utils.createError('Database error'));
    } else if (!user) {
      response.send(utils.createError('user not found'));
    } else {
      const otp = utils.randomNumber()
      user.otp = cryptoJs.SHA256(otp);
      user.otpVerified = false;
      user.save((error, result) => {
        if (error) {
          console.log(error)
          return response.send(utils.createError('Database error'));
        }
        const body = `Hi ${user.firstName}!, Please use the following verification code to reset your password ${otp}`
        let contact = result.isdCode + result.phone;
        mailer.sendSMS(contact, body, (message) => {
          response.send(utils.createSuccess(message))
        })
      })
    }
  })
})

router.post('/verify-otp', (request, response) => {
  const { email, phone, otp } = request.body
  User.findOne({ $or: [{ email }, { phone }] }, (error, user) => {
    if (error) {
      response.send(utils.createError('Database error'));
    } else if (!user) {
      response.send(utils.createError('user not found'));
    } else {

      const userOtp = cryptoJs.SHA256(otp)
      if (userOtp == user.otp) {
        user.otp = ""
        user.phoneVerified = true;
        user.otpVerified = true;
        user.save((error, user) => {
          if (error) {
            console.log(error)
            return response.send(utils.createError('Database error'));
          }
          response.send(utils.createSuccess(user.safeUser()))
        })
      }

    }
  })
})

router.post('/reset-password', (request, response) => {
  const { email, phone, password } = request.body
  if (!password || password.length < 6) {
    console.log("password error", password)
    response.send(utils.createError("Paasword should atleast be 6 character long"));
  } else
    User.findOne({ $or: [{ email }, { phone }] }, (error, user) => {
      if (error) {
        console.log("error find", error)
        response.send(utils.createError('Database error'));
      } else if (!user) {
        response.send(utils.createError('user not found'));
      } else {
        if (user.otpVerified) {
          user.otpVerified = false;
          user.password = cryptoJs.SHA256(password)
          user.save((error, result) => {
            if (error) {
              console.log("error save", error)
              response.send(utils.createError('Database error'));
            } else {
              response.send(utils.createResult(error, result.safeUser()))
            }
          })
        } else {
          response.send(utils.createResult("Invalid request"))
        }
      }
    })
})

router.get('/profile', function (request, response) {

  User.findOne({ _id: request.userId }, (error, user) => {
    if (error) {
      response.send(utils.createError('Database error'));
    } else if (!user) {
      response.send(utils.createError('User not found'));
    } else {

      response.send(utils.createSuccess(user.safeUser()))
    }
  })

})

/* getting the list of all users */
router.get('/users-list', function (request, response) {

  User.find({ deleted: false, role: 'user' }, (error, users) => {
    if (error) {
      response.send(utils.createError('Database error'));
    } else if (!users) {
      response.send(utils.createError('User not found'));
    } else {

      response.send(utils.createSuccess(users.map(u => u.safeUser())))
    }
  })

})

router.post('/update', function (request, response) {
  const { firstName, lastName, phone, gender, latitude, longitude, device_token } = request.body

  const JoiSchema = Joi.object({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    phone: Joi.string().min(10).max(10).required(),
    gender: Joi.string().valid('male')
      .valid('female').valid('other').optional(),
  }).options({ abortEarly: false });
  const { error } = JoiSchema.validate({ firstName, lastName, phone, gender })
  if (error)
    return response.send(utils.createError(error));


  User.findOne({ _id: request.userId }, (error, user) => {
    if (error) {
      response.send(utils.createError('Database error'));
    } else if (!user) {
      response.send(utils.createError('User not found'));
    } else {

      if (firstName) user.firstName = firstName || ""
      if (lastName) user.lastName = lastName || ""

      if (phone && user.phone !== phone) {
        user.phone = phone
        user.phoneVerified = false;
      }
      if (gender) user.gender = gender || ""

      if (device_token) user.token = device_token
      if (latitude && longitude) {
        user.location = {
          type: 'Point',
          coordinates: [longitude, latitude]
        }
        if (locationLable) {
          user.location = {
            type: 'Point',
            coordinates: [longitude, latitude],
            label: locationLable
          }
        }
      }

      user.save((error, result) => {
        console.log(error, result);
        if (error) return response.send(utils.createResult("Database error"))

        response.send(utils.createResult(error, result.safeUser()))
      })
    }
  })

})

router.post('/userImage', upload.single('image'), (request, response) => {
  console.log("userImage", request.file)
  try {
    User
      .findOne({ _id: request.userId, deleted: false })
      .exec((error, user) => {
        if (error) {
          console.log(error)
          response.send(utils.createError(error.message))
        } else if (!user) {
          response.send(utils.createError('user does not exist!'))
        } else if (request.file) {

          console.log('File uploaded successfully.');
          console.log(request.file)

          const file = request.file;
          console.log("extracted", file);
          user.image = `uploads/users/${file.filename}`;
          user.save((error, user) => {
            if (error) {
              response.send(utils.createError(error.message))
            } else if (!user) {
              response.send(utils.createError('user not found'))
            } else {
              response.send(utils.createResult(error, user.safeUser()));
            }
          })
        } else {
          response.send(utils.createError('invalid file'))
        }
      });
  } catch (ex) {
    console.log(ex)
    response.send(utils.createError('Unauthorized: invalid token'))
  }

});


router.delete('/delete-user/:userId', (request, response) => {
  const { userId } = request.params

  User.findOne({ _id: userId }, (err, user) => {
    console.log({
      err, user
    })
    if (err)
      return response.send(utils.createError(err));
    else {
      user.deleted = true;
      user.save((err, result) => {

        if (err) return response.send(utils.createResult("Database err"))

        response.send(utils.createResult(err, result.safeUser()))
      })
    }

  })
})
module.exports = router;
