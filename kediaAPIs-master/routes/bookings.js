const express = require('express')
const router = express.Router()
const path = require('path')
const https = require('https')
const Booking = require('../models/Booking')
const Property = require('../models/Property')
const utils = require('../utils')
const config = require('../config/constants')
const mailer = require('./mailer')
const multer = require('multer')
const moment = require('moment')
const querystring = require('querystring')

const TRANSACTION_STATUS_ID_FAILED = 2;
const TRANSACTION_STATUS_ID_SUCCESS = 3;
// SET STORAGE
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/bookings')
    },
    filename: function (req, file, cb) {
        let a = file.originalname.split('.')
        cb(null, `${file.fieldname}-${Date.now()}.${a[a.length - 1]}`)
    }
})

const upload = multer({
    storage: storage,
    fileFilter: function (req, file, callback) {
        var ext = path.extname(file.originalname);
        if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
            return callback(utils.createError("Only images are allowed!"))
        }
        callback(null, true)
    },
    limits: {
        fileSize: 1024 * 1024
    }
})

router.post('/list', utils.verifyAdmin, (request, response) => {
    try {
        const { start, end } = request.body;
        let condition = { deleted: false }
        if (start && end) {
            condition = { deleted: false, createdAt: { $gte: start, $lte: end } }
        }

        Booking.find(condition, { deleted: 0, __v: 0 })
            .populate({ path: 'user', select: 'fullName email isdCode phone image aadharCard ' })
            .populate({ path: 'property' })
            .sort({ createdAt: -1 })
            .exec((error, bookings) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!bookings) {
                    response.send(utils.createError('bookings not found'))
                } else {
                    response.send(utils.createResult(error, bookings));
                }
            })
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

// transactionStatus: { $ne: 0 },
router.get('/my', (request, response) => {
    try {
        Booking.find({  deleted: false, user: request.userId }, { deleted: 0, __v: 0 })
            .populate({ path: 'user', select: 'fullName,phone,isdCode,email,aadharCard,city,state,country' })
            .populate({ path: 'property', populate: 'project building' })
            .sort({ createdOn: -1 })
            .exec((error, bookings) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!bookings) {
                    response.send(utils.createError('bookings not found'))
                } else {
                    response.send(utils.createResult(error, bookings));
                }
            })
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.get('/details/:id', (request, response) => {
    try {
        const { id } = request.params
        Booking.findOne({ deleted: false, _id: id }, { deleted: 0, __v: 0 })
            .populate({ path: 'user', select: 'fullName email isdCode phone image' })
            .populate({ path: 'property' })
            .sort({ createdOn: -1 })
            .exec((error, bookings) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!bookings) {
                    response.send(utils.createError('bookings not found'))
                } else {
                    response.send(utils.createResult(error, bookings));
                }
            })
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.get('/ticket/:id', (request, response) => {
    try {
        const { id } = request.params
        Booking.find({ deleted: false, ticket: id }, { deleted: 0, __v: 0 })
            .populate({ path: 'user', select: 'fullName email isdCode phone image' })
            .populate({ path: 'property' })
            .sort({ createdOn: -1 })
            .exec((error, bookings) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!bookings) {
                    response.send(utils.createError('bookings not found'))
                } else {
                    response.send(utils.createResult(error, bookings));
                }
            })
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/add', (request, response) => {
    try {
        const { name, phone, property, paidAmount } = request.body;
        if (name, property, paidAmount) {
            Property.findOne({ _id: property }).exec((error, propertyObj) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!propertyObj) {
                    response.send(utils.createError("Property not found!"))
                } else if (propertyObj.isBooked) {
                    response.send(utils.createError("Property already booked!"))
                } else if (propertyObj.bookingPrice !== paidAmount) {
                    response.send(utils.createError("Please pay full booking amount!"))
                } else {
                    let booking = new Booking();
                    booking.name = name;
                    booking.phone = phone;
                    booking.user = request.userId;
                    booking.property = property;
                    booking.paidAmount = paidAmount

                    booking.isNew = true;

                    booking.paymentOption = "booking"


                    console.log("paymentOption", booking.paymentOption)
                    booking.save((error, booking) => {
                        console.log({ error, booking })
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!booking) {
                            response.send(utils.createError('booking not found'))
                        } else {
                            propertyObj.isBooked = true;
                            propertyObj.bookingId = booking;
                            propertyObj.save((error, result) => {
                                console.log({ error, result })
                                if (error) {
                                    response.send(utils.createError(error))
                                } else if (!result) {
                                    response.send(utils.createError('prperty not found'))
                                } else {
                                    response.send(utils.createResult(error, booking));
                                }
                            })
                        }
                    })
                }
            })
        } else {
            response.send(utils.createError('Invalid Transaction Request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/offline/add', (request, response) => {
    try {
        const { name, phone, user, property } = request.body;
        if (name, property, totalAmount) {

            let booking = new Booking();
            booking.name = name;
            booking.phone = phone;
            booking.user = request.userId;
            booking.property = property;
            booking.isOffline = true;

            booking.isNew = true;

            booking.save((error, booking) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!booking) {
                    response.send(utils.createError('booking not found'))
                } else {
                    response.send(utils.createResult(error, booking));
                }
            })

        } else {
            response.send(utils.createError('Invalid Transaction Request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/update/:bookingId', (request, response) => {
    try {
        const { bookingId } = request.params;
        const { name, phone, user, property, paidAmount } = request.body;
        if (bookingId) {
            Property.findOne({ _id: property }).exec((error, propertyObj) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!propertyObj) {
                    response.send(utils.createError("Property not found!"))
                } else {
                    Booking.findOne({ _id: bookingId }).exec((error, booking) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!booking) {
                            response.send(utils.createError("Booking not found!"))
                        } else if (booking.isOffline) {
                            response.send(utils.createError("can not edit offline booking!"))
                        } else {
                            let oldProperty;
                            if (name) booking.name = name;
                            if (phone) booking.phone = phone;
                            //if(user) booking.user = request.userId;
                            if (property && booking.property !== property) {
                                oldProperty = booking.property;
                                booking.property = property;
                            }
                            if (paidAmount) booking.paidAmount = paidAmount

                            console.log("paymentOption", booking.paymentOption)
                            booking.save((error, booking) => {
                                console.log({ error, booking })
                                if (error) {
                                    response.send(utils.createError(error))
                                } else if (!booking) {
                                    response.send(utils.createError('booking not found'))
                                } else {
                                    if (oldProperty) {
                                        Property.findOne({ _id: oldProperty })
                                            .exec(async (error, oldProp) => {
                                                if (oldProp) {
                                                    oldProp.isBooked = false;
                                                    delete oldProp.bookingId;
                                                    await oldProp.save();
                                                }
                                            })
                                        propertyObj.isBooked = true;
                                        propertyObj.bookingId = booking;
                                        propertyObj.save((error, result) => {
                                            console.log({ error, result })
                                            if (error) {
                                                response.send(utils.createError(error))
                                            } else if (!result) {
                                                response.send(utils.createError('prperty not found'))
                                            } else {
                                                response.send(utils.createResult(error, booking));
                                            }
                                        })
                                    } else {
                                        response.send(utils.createResult(error, booking));
                                    }
                                }
                            })
                        }
                    });
                }
            })

        } else {
            response.send(utils.createError('Invalid Transaction Request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/offline/update/:bookingId', (request, response) => {
    try {
        const { bookingId } = request.params;
        const { name, phone, user, property } = request.body;
        if (bookingId) {

            Booking.findOne({ _id: bookingId, isOffline: true }).exec((error, booking) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!booking) {
                    response.send(utils.createError("Booking not found!"))
                } else if (!booking.isOffline) {
                    response.send(utils.createError("This is not an offline booking!"))
                } else {
                    booking.name = name;
                    booking.phone = phone;
                    // booking.user = request.userId;
                    booking.property = property;

                    booking.save((error, booking) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!booking) {
                            response.send(utils.createError('booking not found'))
                        } else {
                            response.send(utils.createResult(error, booking));
                        }
                    })
                }
            });
        } else {
            response.send(utils.createError('Invalid Transaction Request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/transaction/update/:id', (request, response) => {
    try {
        const { id } = request.params;
        const { transactionId, transactionStatus, transactionAmount, responseObject } = request.body
        if (id) {
            Booking.findOne({ _id: id }, { deleted: 0, __v: 0 }, (error, booking) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!booking) {
                    response.send(utils.createError('booking not found'))
                } else {

                    if (transactionId) booking.transactionId = transactionId;
                    if (transactionStatus) booking.transactionStatus = transactionStatus;
                    if (transactionAmount) booking.transactionAmount = transactionAmount;
                    if (responseObject) booking.responseObject = JSON.parse(responseObject);
                    booking.payment = transactionStatus === 3 ? "Success" : "Failed"
                    booking.save((error, booking) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!booking) {
                            response.send(utils.createError('booking not found'))
                        } else {

                            let body = `Your payment successful for booking at SporTime`;
                            if (booking.transactionStatus === TRANSACTION_STATUS_ID_FAILED) {
                                body = `Your payment failed for booking at SporTime`;
                            } else {
                                body = `Your payment successful for booking at Sport-Time`;
                            }

                            mailer.sendSMS(booking.phone, body, (message) => {
                                console.log(message);
                                let smsMessage = `Hello ${booking.name}!, ${body}.Team SporTime`
                                mailer.sendSMS(user.isdCode + user.phone, smsMessage, (smsError, data) => {
                                    console.log("Email", smsError, data)
                                    response.send(utils.createResult(error, booking));
                                })

                            })
                        }
                    })
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/cancel/:id', (request, response) => {
    console.log(request.file)
    try {
        const { id } = request.params;
        if (id) {
            Booking.findOne({ _id: id }, { deleted: 0, __v: 0 }, (error, booking) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!booking) {
                    response.send(utils.createError('booking not found'))
                } else {

                    booking.status = "cancelled"

                    booking.save((error, booking) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!booking) {
                            response.send(utils.createError('booking not found'))
                        } else {
                            response.send(utils.createResult(error, booking));
                        }
                    })
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.delete('/delete/:id', (request, response) => {
    console.log(request.file)
    try {
        const { id } = request.params;
        if (id) {
            Booking.findOne({ _id: id }, { deleted: 0, __v: 0 }, (error, booking) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!booking) {
                    response.send(utils.createError('booking not found'))
                } else {

                    booking.deleted = true;
                    booking.save((error, booking) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!booking) {
                            response.send(utils.createError('booking not found'))
                        } else {
                            Property.findOne({ _id: booking.property }).exec((err, property) => {
                                property.isBooked = false
        
                                property.save();
                            })
                            response.send(utils.createSuccess(booking));
                        }
                    })
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

module.exports = router;