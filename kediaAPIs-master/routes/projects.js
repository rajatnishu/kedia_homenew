const express = require("express");
const router = express.Router();
const path = require("path");
const User = require("../models/User");
const Property = require("../models/Property");
const Building = require("../models/Building");
const Project = require("../models/Project");
const utils = require("../utils");
const Joi = require("@hapi/joi");
const cryptoJs = require("crypto-js");
const jwt = require("jsonwebtoken");
const config = require("../config/constants");
const mailer = require("./mailer");
const multer = require('multer')

// SET STORAGE/ 
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    console.log("destination", file);
    try {
      cb(null, "public/uploads/property");
    } catch (e) {
      cb(e);
    }
  },
  filename: function (req, file, cb) {
    console.log("filename", file);
    try {
      let a = file.originalname.split(".");
      cb(null, `${Date.now()}.${a[a.length - 1]}`);
    } catch (e) {
      cb(e);
    }
  },
});

const upload = multer({
  storage: storage,
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    console.log({ file, ext }, file.fieldname === "images360" && ext !== '.mp4' && ext !== '.webm' && ext !== '.mkv')
    if (file.fieldname === "images360") {
      if (ext !== '.mp4' && ext !== '.webm' && ext !== '.mkv')
        return callback("Only videos with MP4, WEBM, and MKV extentions are allowed!")
      else callback(null, true)
    } else if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
      return callback("Only images with PNG, JPG, GIF, and JPEG extentions are allowed!")
    }
    console.log("fileFilter");
    callback(null, true);
  },
  limits: {
    fileSize: 1024 * 1024 * 10,
  },
});

router.post("/property/list", function (request, response) {
  const { project, building, floor, type } = request.body
  let condition = [{ deleted: false }]
  if (project) condition = [{ deleted: false, project: project }]
  if (building) condition.push({ building })
  if (floor) condition.push({ floor })
  if (type) condition.push({ type })

  Property.find({ $and: condition }, (error, properties) => {
    if (error) {
      response.send(utils.createError("Database error"));
    } else if (!properties || properties.length === 0) {
      response.send(utils.createError("No properties found!"));
    } else {
      response.send(utils.createResult(error, properties));
    }
  });
});

router.post("/property/add", upload.fields([
  {
    name: 'images', maxCount: 5
  }, {
    name: 'planImage', maxCount: 1
  },
  {
    name: 'images360', maxCount: 5
  }
]), function (request, response) {
  const {
    type,
    description,
    areaSqft,
    serial,
    bedrooms,
    bathrooms,
    livingRoom,
    balcony,
    price,
    bookingPrice,
    floor,
    building,
    project,
    possessionStatus,
    perSqFtPrice
  } = request.body;

  const JoiSchema = Joi.object({

    description: Joi.string().min(10).required(),
    areaSqft: Joi.number().required(),
    serial: Joi.string().max(10).required(),
    bedrooms: Joi.alternatives().conditional('type', { is: 'Flat', then: Joi.number().min(1).max(10).required() }), 
    bathrooms: Joi.alternatives().conditional('type', { is: 'Flat', then: Joi.number().min(1).max(10).required() }),
    livingRoom: Joi.alternatives().conditional('type', { is: 'Flat', then: Joi.number().min(1).max(10).required() }),
    balcony: Joi.boolean().optional(),
    type: Joi.string().valid('Plot')
      .valid('Flat').required(),
    price: Joi.number().required(),
    bookingPrice: Joi.number().required(),
    building: Joi.string().min(24).max(24),
    floor: Joi.number().optional(),
    project: Joi.string().min(24).max(24).required(),
    possessionStatus: Joi.string().required(),
    perSqFtPrice: Joi.string().required()

  }).options({ abortEarly: false });
  const { error } = JoiSchema.validate({
    type,
    description,
    areaSqft,
    serial,
    bedrooms,
    bathrooms,
    livingRoom,
    balcony,
    price,
    bookingPrice,
    floor,
    building,
    project,
    possessionStatus,
    perSqFtPrice
  })

  if (error) {
    return response.send(utils.createError(error));
  }
  let property = new Property(request.body);

  console.log({
    property: property
  })

  if (request.files) {
    console.log('File uploaded successfully.');

    let files = request.files['images'];
    console.log({ images: files && files.length })
    if (files) {
      console.log("images", files.length);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        // console.log("extracted", file);
        property.images.push(`uploads/property/${file.filename}`);
      }
    }

    files = request.files['planImage'];
    console.log({ planImage: files && files.length })
    if (files) {
      console.log("planImage", files.length);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        // console.log("extracted", file);
        property.planImage = `uploads/property/${file.filename}`;
      }
    }

    files = request.files['images360'];
    console.log({ images360: files && files.length })
    if (files) {
      console.log("images360", files.length);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        property.images360 = `uploads/property/${file.filename}`;
      }
    }
  }

  property.save(async (error, result) => {
    if (error) {
      console.log({
        error
      })
      response.send(utils.createError("Database error"));
    } else if (!result) {
      response.send(utils.createError("Property not found!"));
    } else {
      response.send(utils.createResult(error, result));
    }
  });
});

router.post("/property/update/:propertyId", upload.fields([
  {
    name: 'images', maxCount: 5
  },
  {
    name: 'planImage', maxCount: 1
  },
  {
    name: 'images360', maxCount: 5
  }
]), async function (request, response) {
  const { propertyId } = request.params
  const {
    type,
    description,
    areaSqft,
    serial,
    bedrooms,
    bathrooms,
    livingRoom,
    balcony,
    facing,
    price,
    bookingPrice,
    floor,
    building,
    project
  } = request.body;

  const JoiSchema = Joi.object({
    description: Joi.string().min(10).required(),
    areaSqft: Joi.number().required(),
    serial: Joi.string().max(10).required(),
    bedrooms: Joi.alternatives().conditional('type', { is: 'Flat', then: Joi.number().min(1).max(10).required() }),
    bathrooms: Joi.alternatives().conditional('type', { is: 'Flat', then: Joi.number().min(1).max(10).required() }),
    livingRoom: Joi.alternatives().conditional('type', { is: 'Flat', then: Joi.number().min(1).max(10).required() }),
    balcony: Joi.boolean().optional(),
    type: Joi.string().valid('Plot')
      .valid('Flat').required(),
    facing: Joi.string().valid('East')
      .valid('West').valid('North').valid('South').required(),
    price: Joi.number().required(),
    bookingPrice: Joi.number().required(),
    building: Joi.string().min(24).max(24),
    floor: Joi.number().optional(),
    project: Joi.string().min(24).max(24).required()

  }).options({ abortEarly: false });
  const { error } = JoiSchema.validate({
    type,
    description,
    areaSqft,
    serial,
    bedrooms,
    bathrooms,
    livingRoom,
    balcony,
    facing,
    price,
    bookingPrice,
    floor,
    building,
    project
  })

  if (error) {
    return response.send(utils.createError(error));
  }
  let property = await Property.findByIdAndUpdate(propertyId, request.body);

  if (request.files) {
    console.log('File uploaded successfully.');

    let files = request.files['images'];
    console.log({ images: files && files.length })
    if (files) {
      console.log("images", files.length);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        // console.log("extracted", file);
        property.images.push(`uploads/property/${file.filename}`);
      }
    }
    files = request.files['planImage'];
    console.log({ planImage: files && files.length })
    if (files) {
      console.log("planImage", files.length);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        // console.log("extracted", file);
        property.planImage = `uploads/property/${file.filename}`;
      }
    }

    files = request.files['images360'];
    console.log({ images360: files && files.length })
    if (files) {
      console.log("images360", files.length);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        property.images360 = `uploads/property/${file.filename}`;
      }
    }
  }

  property.save(async (error, result) => {
    if (error) {
      response.send(utils.createError("Database error"));
    } else if (!result) {
      response.send(utils.createError("Property not found!"));
    } else {
      response.send(utils.createResult(error, result));
    }
  });
});

router.get("/property/detail/:propertyId", async function (request, response) {
  const { propertyId } = request.params

  Property.findById(propertyId)
    .populate('project building')
    .exec(async (error, result) => {
      if (error) {
        response.send(utils.createError("Database error"));
      } else if (!result) {
        response.send(utils.createError("Property not found!"));
      } else {
        response.send(utils.createResult(error, result));
      }
    });
});


/* deleting property/flat */
router.delete("/property/delete/:propertyId", function (request, response) {
  const { propertyId } = request.params

  Property.findById(propertyId, async (error, property) => {
    property.deleted = true;
    property.save(async (error, result) => {
      console.log({ error, result })
      if (error) {
        response.send(utils.createError("Database error"));
      } else if (!result) {
        response.send(utils.createError("Plot/Flat not found!"));
      } else {
        response.send(utils.createResult(error, result));
      }
    });
  })
});


/* ---------------------------------------------BUILDING------------------------------------------------------------------------------ */

router.get("/building/list/:projectId", function (request, response) {
  const { projectId } = request.params
  Building.find({ project: projectId, deleted: false }, (error, buildings) => {
    if (error) {
      response.send(utils.createError("Database error"));
    } else if (!buildings || buildings.length === 0) {
      response.send(utils.createError("No Building found!"));
    } else {
      response.send(utils.createResult(error, buildings));
    }
  });
});

router.get("/building/detail/:buildingId", function (request, response) {
  const { buildingId } = request.params
  Building.findOne({ _id: buildingId, deleted: false })
    .populate('project')
    .exec(async (error, building) => {
      if (error) {
        response.send(utils.createError("Database error"));
      } else if (!building) {
        response.send(utils.createError("No Building found!"));
      } else {
        let parsedBulding = JSON.parse(JSON.stringify(building))
        parsedBulding['property'] = await Property.find({ building: buildingId, deleted: false, type: 'Flat' })
        response.send(utils.createSuccess(parsedBulding));
      }
    });
});

router.post("/building/add", upload.array('images'), function (request, response) {

  const {
    name,
    description,
    floors,
    flats,
    lifts,
    serviceLift,
    project
  } = request.body;

  const JoiSchema = Joi.object({
    name: Joi.string().min(6).required(),
    description: Joi.string().allow('').min(10).optional(),
    floors: Joi.number().optional(),
    flats: Joi.number().optional(),
    lifts: Joi.number().optional(),
    serviceLift: Joi.boolean().optional(),
    project: Joi.string().min(24).max(24).required(),
  }).options({ abortEarly: false });
  const { error } = JoiSchema.validate({
    name,
    description,
    floors,
    lifts,
    flats,
    serviceLift,
    project
  })

  if (error) {
    return response.send(utils.createError(error));
  }
  let building = new Building(request.body);

  if (request.files) {
    console.log('File uploaded successfully.');
    console.log({ files: request.files.length })

    for (let i = 0; i < request.files.length; i++) {
      let file = request.files[i];
      // console.log("extracted",file);
      building.images.push(`uploads/property/${file.filename}`);
    }
  }

  building.save(async (error, result) => {
    console.log({
    error, result
    })
    if (error) {
      response.send(utils.createError("Database error"));
    } else if (!result) {
      response.send(utils.createError("Building not found!"));
    } else {
      response.send(utils.createResult(error, result));
    }
  });
});

router.delete("/building/delete/:buildingId", function (request, response) {
  const { buildingId } = request.params

  Building.findById(buildingId, async (error, building) => {
    building.deleted = true;
    building.save(async (error, result) => {
      console.log({ error, result })
      if (error) {
        response.send(utils.createError("Database error"));
      } else if (!result) {
        response.send(utils.createError("Building not found!"));
      } else {
        response.send(utils.createResult(error, result));
      }
    });
  })
});

router.post("/building/update/:buildingId", upload.array('images'), function (request, response) {
  const { buildingId } = request.params

  const {
    name,
    description,
    floors,
    flats,
    lifts,
    serviceLift,
    project
  } = request.body;

  const JoiSchema = Joi.object({
    name: Joi.string().min(6).required(),
    description: Joi.string().allow('').min(10).optional(),
    floors: Joi.number().optional(),
    flats: Joi.number().optional(),
    lifts: Joi.number().optional(),
    serviceLift: Joi.boolean().optional(),
    project: Joi.string().min(24).max(24).required(),
  }).options({ abortEarly: false });
  const { error } = JoiSchema.validate({
    name,
    description,
    floors,
    lifts,
    flats,
    serviceLift,
    project
  })

  if (error) {
    return response.send(utils.createError(error));
  }

  Building.findById(buildingId, async (error, building) => {

    let updatedBuilding = JSON.parse(JSON.stringify(building));

    building = Object.assign(building, request.body)
    if (request.files) {
      console.log('File uploaded successfully.');
      console.log({ files: request.files.length })
      building.images = []
      for (let i = 0; i < request.files.length; i++) {
        let file = request.files[i];
        // console.log("extracted",file);
        building.images.push(`uploads/property/${file.filename}`);
      }
    }
    building.save(async (error, result) => {
      console.log({ error, result })
      if (error) {
        response.send(utils.createError("Database error"));
      } else if (!result) {
        response.send(utils.createError("Building not found!"));
      } else {
        response.send(utils.createResult(error, result));
      }
    });
  })
});

/* -----------------------------------------------------PROJECTS------------------------------------------------------------------------- */
router.get("/list", function (request, response) {
  Project.find({ deleted: false })
    .exec(async (error, projects) => {
      console.log({
        error, p: projects && projects.length
      })
      if (error) {

        response.send(utils.createError("Database error"));
      } else if (!projects || projects.length === 0) {
        response.send(utils.createError("No project found!"));
      } else {
        response.send(utils.createResult(error, projects));
      }
    });
});

router.get("/list-full-details", function (request, response) {
  Project.find({ deleted: false })
    .populate('amenities')
    .exec(async (error, projects) => {
      console.log({
        error, p: projects && projects.length
      })
      if (error) {

        response.send(utils.createError("Database error"));
      } else if (!projects || projects.length === 0) {
        response.send(utils.createError("No project found!"));
      } else {
        response.send(utils.createResult(error, projects));
      }
    });
});

router.post("/add", upload.fields([
  {
    name: 'images'
  },
  {
    name: 'logo', maxCount: 1
  },
  {
    name: 'planMap', maxCount: 1
  },
  {
    name: 'broucher', maxCount: 1
  }
]), function (request, response) {
  const { name,
    description,
    amenities,
    address,
    city,
    state,
    country,
    numberOfHouses,
    numberOfBuildings,
    latitude,
    longitude,
    area,
    launchDate,
    reraId,
    bankDetails,
    startsFrom,

    category,
    keyfeatures,
    whyInvest,
    bhkNums,
    planMap,
    broucher,
    floorings,
    fittings,
    walls,
    avgPrice,
    typesOfFlats
  } = request.body;

  const JoiSchema = Joi.object({
    description: Joi.string().allow('').max(500).optional(),
    name: Joi.string().min(6).required(),
    amenities: Joi.array().required(),
    address: Joi.string().allow('').min(6).optional(),
    city: Joi.string().allow('').min(6).optional(),
    state: Joi.string().allow('').min(6).optional(),
    country: Joi.string().allow('').optional(),
    numberOfBuildings: Joi.number().optional(),
    numberOfHouses: Joi.number().optional(),
    latitude: Joi.number().optional(),
    longitude: Joi.number().optional(),
    area: Joi.string().optional(),
    launchDate: Joi.string().optional(),
    reraId: Joi.string().optional(),
    bankDetails: Joi.array().optional(),
    startsFrom: Joi.string().required(),

    category: Joi.string().optional(),
    bhkNums: Joi.string().optional(),
    keyfeatures: Joi.string().optional(),
    whyInvest: Joi.string().optional(),
    floorings: Joi.string().optional(),
    fittings: Joi.string().optional(),
    walls: Joi.string().optional(),
    avgPrice: Joi.string().optional(),
    typesOfFlats: Joi.string().optional()

  }).options({ abortEarly: false });
  const { error } = JoiSchema.validate({
    name,
    description,
    amenities,
    address,
    city,
    state,
    country,
    numberOfBuildings,
    numberOfHouses,
    latitude,
    longitude,
    area,
    launchDate,
    reraId,
    bankDetails,
    startsFrom,
    category,
    bhkNums,
    keyfeatures,
    whyInvest,
    floorings,
    fittings,
    walls,
    avgPrice,
    typesOfFlats
  })

  if (error) {
    return response.send(utils.createError(error));
  }
  let project = new Project({
    name,
    description,
    amenities: amenities,
    address,
    city,
    state,
    country,
    numberOfBuildings,
    numberOfHouses,
    latitude,
    longitude,
    area,
    launchDate,
    reraId,
    bankDetails,
    startsFrom,

    category,
    bhkNums: typeof bhkNums === 'object' ? bhkNums : JSON.parse(bhkNums),
    keyfeatures: keyfeatures && keyfeatures.split(","),
    whyInvest: whyInvest && whyInvest.split(","),
    floorings: floorings && floorings.split(","),
    fittings: fittings && fittings.split(","),
    walls: walls && walls.split(","),

    avgPrice: avgPrice,
    typesOfFlats: typesOfFlats,
  });

  if (latitude && longitude) {
    project.location = {
      type: 'Point',
      coordinates: [latitude, longitude]
    }
  }

  if (request.files) {
    console.log('File uploaded successfully.');

    let files = request.files['images'];
    console.log({ images: files.length })
    if (files) {
      console.log("images", files.length);
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        // console.log("extracted", file);
        project.images.push(`uploads/property/${file.filename}`);
      }
    }

    files = request.files['logo'];
    if (files && files.length > 0) {
      project.logo = `uploads/property/${files[0].filename}`
    }

    files = request.files['planMap'];
    if (files && files.length > 0) {
      project.planMap = `uploads/property/${files[0].filename}`
    }


    files = request.files['broucher'];
    if (files && files.length > 0) {
      project.broucher = `uploads/property/${files[0].filename}`
    }
  }

  project.save(async (error, result) => {
    console.log({ error, result })
    if (error) {
      response.send(utils.createError("Database error"));
    } else if (!result) {
      response.send(utils.createError("Project not found!"));
    } else {
      response.send(utils.createResult(error, result));
    }
  });
});

/* For updating project  */
router.post("/update/:projectId", upload.fields([
  {
    name: 'images'
  },
  {
    name: 'logo',
    maxCount: 1
  },
  {
    name: 'planMap', maxCount: 1
  },
  {
    name: 'broucher', maxCount: 1
  }
]), function (request, response) {
  const { name,
    description,
    amenities,
    address,
    city,
    state,
    country, deleteImages,
    numberOfBuildings,
    numberOfHouses,
    area,
    launchDate,
    reraId,
    bankDetails,
    startsFrom,

    category,
    keyfeatures,
    whyInvest,
    bhkNums,
    planMap,
    broucher,
    floorings,
    fittings,
    walls,
    avgPrice,
    typesOfFlats
  } = request.body;

  const { projectId } = request.params;

  const JoiSchema = Joi.object({
    description: Joi.string().allow('').max(500).optional(),
    name: Joi.string().min(6).optional(),
    amenities: Joi.string().optional(),
    address: Joi.string().allow('').min(6).optional(),
    city: Joi.string().allow('').min(6).optional(),
    state: Joi.string().allow('').min(6).optional(),
    country: Joi.string().allow('').optional(),
    numberOfBuildings: Joi.number().optional(),
    numberOfHouses: Joi.number().optional(),
    latitude: Joi.number().optional(),
    longitude: Joi.number().optional(),

    area: Joi.string().optional(),
    launchDate: Joi.string().optional(),
    reraId: Joi.string().optional(),
    bankDetails: Joi.string().optional(),
    startsFrom: Joi.string().optional(),

    category: Joi.string().optional(),
    bhkNums: Joi.string().optional(),
    keyfeatures: Joi.string().optional(),
    whyInvest: Joi.string().optional(),
    floorings: Joi.string().optional(),
    fittings: Joi.string().optional(),
    walls: Joi.string().optional(),
    avgPrice: Joi.string().optional(),
    typesOfFlats: Joi.string().optional()

  }).options({ abortEarly: false });
  const { error } = JoiSchema.validate({
    name,
    description,
    amenities,
    address,
    city,
    state,
    numberOfBuildings,
    numberOfHouses,
    country,
    area,
    launchDate,
    reraId,
    bankDetails,
    startsFrom,

    category,
    bhkNums,
    keyfeatures,
    whyInvest,
    floorings,
    fittings,
    walls,
    avgPrice,
    typesOfFlats
  })

  if (error) {
    return response.send(utils.createError(error));
  }

  console.log({ body: request.body })
  Project.findById(projectId, (err, project) => {
    console.log({
    err,project
    })
    project.name = name,
      project.description = description;
      project.amenities = amenities;
      project.address = address;
      project.city = city;
      project.state = state;
      project.country = country;
      project.numberOfBuildings = numberOfBuildings;
      project.numberOfHouses = numberOfHouses;
      if(deleteImages) project.images = deleteImages,

      project.area = area,
      project.launchDate = launchDate,
      project.reraId = reraId,
      project.bankDetails = bankDetails,
      project.startsFrom = startsFrom,

      project.category = category,
      project.bhkNums = typeof bhkNums === 'object' ? bhkNums : JSON.parse(bhkNums),
      project.keyfeatures = keyfeatures && keyfeatures.split(","),
      project.whyInvest = whyInvest && whyInvest.split(","),
      project.floorings = floorings && floorings.split(","),
      project.fittings = fittings && fittings.split(","),
      project.walls = walls && walls.split(","),
      project.avgPrice = avgPrice,
      project.typesOfFlats = typesOfFlats

    if (request.files) {
      console.log('File uploaded successfully.');

      let files = request.files['images'];
      console.log({ images: files && files.length })
      if (files) {
        console.log("images", files.length);
        for (let i = 0; i < files.length; i++) {
          let file = files[i];

          project.images.push(`uploads/property/${file.filename}`);
        }
      }

      files = request.files['logo'];
      if (files && files.length > 0) {
        project.logo = `uploads/property/${files[0].filename}`
      }

      files = request.files['planMap'];
      if (files && files.length > 0) {
        project.planMap = `uploads/property/${files[0].filename}`
      }


      files = request.files['broucher'];
      if (files && files.length > 0) {
        project.broucher = `uploads/property/${files[0].filename}`
      }

    }

    project.save(async (error, result) => {
      console.log({ error, result })
      if (error) {
        response.send(utils.createError("Database error"));
      } else if (!result) {
        response.send(utils.createError("Project not found!"));
      } else {
        response.send(utils.createResult(error, result));
      }
    });
  });


});

router.delete("/delete/:projectId", upload.array('images'), function (request, response) {
  const { projectId } = request.params

  Project.findById(projectId, async (error, project) => {
    project.deleted = true;
    project.save(async (error, result) => {
      console.log({ error, result })
      if (error) {
        response.send(utils.createError("Database error"));
      } else if (!result) {
        response.send(utils.createError("Project not found!"));
      } else {
        response.send(utils.createResult(error, result));
      }
    });
  })
});

router.post("/property/search", function (request, response) {
  const { searchText, project, building, floor, type, city, state, bedrooms, bathrooms, balcony, area, price } = request.body
  let conditions = [{ deleted: false, published: true }]

  if (project) conditions.push({ project: project })
  if (building) conditions.push({ building })
  if (floor) conditions.push({ floor })

  if (type) conditions.push({ type })
  if (bedrooms) conditions.push({ bedrooms: { $gte: bedrooms } })
  if (bathrooms) conditions.push({ bathrooms: { $gte: bathrooms } })
  if (price) conditions.push({ price: { $gte: price } })
  if (city) conditions.push({ city })
  if (state) conditions.push({ state })
  if (balcony) conditions.push({ balcony })
  if (area) conditions.push({ area: { $gte: area } })

  if (searchText) {
    conditions.push({ address: new RegExp('.*' + searchText + '*.', 'i') })
      .push({ city: new RegExp('.*' + searchText + '*.', 'i') })
      .push({ description: new RegExp('.*' + searchText + '*.', 'i') })
  }

  Property.find({ $and: conditions }, (error, properties) => {
    if (error) {
      response.send(utils.createError("Database error"));
    } else if (!properties || properties.length === 0) {
      response.send(utils.createError("No properties found!"));
    } else {
      response.send(utils.createResult(error, properties));
    }
  });
});


router.get("/details/:projectId", function (request, response) {
  const { projectId } = request.params
  Project.findOne({ _id: projectId, deleted: false })
    .exec(async (error, project) => {
      if (error) {
        response.send(utils.createError("Database error"));
      } else if (!project) {
        response.send(utils.createError("No Project found!"));
      } else {
        response.send(utils.createSuccess(project));
      }
    });
})

/* details for showing details front end */
router.get("/details-full/:projectId", function (request, response) {
  const { projectId } = request.params
  Project.findOne({ _id: projectId, deleted: false })
    .populate('amenities bankDetails')
    .exec(async (error, project) => {
      if (error) {
        response.send(utils.createError("Database error"));
      } else if (!project) {
        response.send(utils.createError("No Project found!"));
      } else {
        response.send(utils.createSuccess(project));
      }
    });
})

router.get("/change-status/:type/:typeId", function (request, response) {
  const { type, typeId } = request.params;

  console.log({
    a: request.params
  })

  let modal;

  switch (type) {
    case 'project':
      modal = Project;
      break;

    case 'building':
      modal = Building
      break

    default:
      modal = Property
      break;
  }
  console.log({
    modal
  })
  modal.findById(typeId, (err, type) => {
    if (err) {
      response.send(utils.createError(err));
    } else {
      console.log({
        type: type
      })
      type.published = !type.published

      type.save((err, type) => {
        if (err) {
          response.send(utils.createError(err));
        } else {
          response.send(utils.createSuccess(type));
        }
      });
    }
  });

})

/* for changing the category of project new or best */
router.get("/change-category/:projectId/:category", (req, res) => {
  const { projectId, category } = req.params

  Project.findOneAndUpdate((err, project) => {
    if (err) {

    }
  })
})
module.exports = router