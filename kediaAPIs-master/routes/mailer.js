const axios = require('axios')
const nodemailer = require("nodemailer");
const admin = require("firebase-admin");
const twilio = require('twilio')
const config = require('../config/constants')
const accountSid = 'accountSid'
const authToken = 'authToken'

async function sendSMS(number, body, callback) {
        console.log("sendSMS",number,body)
        try{
        const client = new twilio(accountSid, authToken);
        // let response = await client.messages.create({
        //     body: body,
        //     to: number,
        //     from: '+12514234536'
        // })
        callback("SMS Sent to "+number + body)
      }catch(ex){
        console.log("sendSMS catch",ex)
        callback("Error Sending SMS to "+number + body)
      }
        // .then((message) => {
        //   console.log(message.sid)
        //   response.send(utils.createSuccess({otp: otp}))
        // })
}

// Send a message to the device corresponding to the provided
// registration token.
async function sendNotification(title,body,intent,token,callback){
    console.log("sendNotification",title,body,intent,token)
    try{
        const message = {
            notification: {
                title: title,
                body: body
              },
              data: {
                title: title,
                body: body
              },
              android: {
                notification: {
                  sound: 'default',
                  click_action: intent || "",
                },
              },
              apns: {
                payload: {
                  aps: {
                    badge: 1,
                    sound: 'default'
                  },
                },
              },
              token
            };
        let response = await admin.messaging().send(message)
        console.log("notification",response)
        callback(null,response)
    }catch(ex){
        console.log("notification Error",ex);
        callback("Error: Notification Failed!",null)
    }    
}

async function sendEmail(to, subject, html, callback) {
  try{
    // create transporter object with smtp server details
   // const transporter = nodemailer.createTransport(config.smtpOptions);
    let from = 'Team Kedia Homes <hello@kedia.com>';
    //let email = await transporter.sendMail({ from, to, subject, html });
    callback(null,"Done")
  }catch(ex){
    console.log("Email Error",ex)
    callback("Error sending email!")
  }
}

// {
//   from: 'from_address@example.com',
//   to: 'to_address@example.com',
//   subject: 'Test Email Subject',
//   html: '<h1>Example HTML Message Body</h1>'
// }

module.exports = {    
    sendSMS: sendSMS,
    sendNotification: sendNotification,
    sendEmail:sendEmail
}



