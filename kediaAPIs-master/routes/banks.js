const express = require('express')
const router = express.Router()
const path = require('path')
const Bank = require('../models/Banks')
const utils = require('../utils')
const multer = require('multer')


// SET STORAGE


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads/banks')
    },
    filename: function (req, file, cb) {
        let a = file.originalname.split('.')
        cb(null, `${file.fieldname}-${Date.now()}.${a[a.length - 1]}`)
    }
})

const upload = multer({
    storage: storage,
    fileFilter: function (req, file, callback) {
        var ext = path.extname(file.originalname);
        if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
            return callback(utils.createError("Only images are allowed!"))
        }
        callback(null, true)
    },
    limits: {
        fileSize: 1024 * 1024 * 2
    }
})

router.get('/list', (request, response) => {
    console.log(request.file)
    try {
        Bank.find({ deleted: false }, { deleted: 0, __v: 0, createdOn: 0 }, (error, banks) => {
            if (error) {
                response.send(utils.createError(error))
            } else if (!banks) {
                response.send(utils.createError('banks not found'))
            } else {
                response.send(utils.createResult(error, banks));
            }
        })
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.get('/get/:id', (request, response) => {
    try {
        const { id } = request.params;
        if (id) {
            Bank.findOne({ _id: id }, { deleted: 0, __v: 0, createdOn: 0 }, (error, bank) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!bank) {
                    response.send(utils.createError('bank not found'))
                } else {
                    response.send(utils.createResult(error, bank));
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/add', upload.single('logo'), (request, response) => {
    console.log(request.file)
    try {
        const {
            name,
            logo,
            loanPercent,
            status
        } = request.body;
        if (name) {
            let bank = new Bank();

            bank.name = name
            bank.logo = logo
            bank.loanPercent = loanPercent
            bank.status = status

            if (request.file) bank.logo = `uploads/banks/${request.file.filename}`;

            bank.save((error, bank) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!bank) {
                    response.send(utils.createError('bank not found'))
                } else {
                    response.send(utils.createResult(error, bank));
                }
            })
        } else {
            response.send(utils.createError('name is required'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/update/:id', upload.single('logo'), (request, response) => {
    console.log(request.file)
    try {
        const { id } = request.params;
        const {
            name,
            logo,
            loanPercent,
            status
        } = request.body
        if (id) {
            Bank.findOne({ _id: id }, { deleted: 0, __v: 0, createdOn: 0 }, (error, bank) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!bank) {
                    response.send(utils.createError('bank not found'))
                } else {

                    if (name) bank.name = name
                    if (loanPercent) bank.loanPercent = loanPercent
                    if (status) bank.status = status
                    if (request.file) bank.logo = `uploads/banks/${request.file.filename}`;

                    bank.save((error, bank) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!bank) {
                            response.send(utils.createError('Bank not found'))
                        } else {
                            response.send(utils.createResult(error, bank));
                        }
                    })
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.delete('/delete/:id', (request, response) => {
    console.log(request.file)
    try {
        const { id } = request.params;
        if (id) {
            Bank.findOne({ _id: id }, { deleted: 0, __v: 0, createdOn: 0 }, (error, bank) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!bank) {
                    response.send(utils.createError('bank not found'))
                } else {

                    bank.deleted = true;

                    bank.save((error, bank) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!bank) {
                            response.send(utils.createError('bank not found'))
                        } else {
                            response.send(utils.createSuccess(bank));
                        }
                    })
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

router.post('/change-status/:id', (request, response) => {
    try {
        const { id } = request.params;
        const { status } = request.body;
        if (id) {
            Bank.findOne({ _id: id }, { deleted: 0, __v: 0, createdOn: 0 }, (error, bank) => {
                if (error) {
                    response.send(utils.createError(error))
                } else if (!bank) {
                    response.send(utils.createError('bank not found'))
                } else {

                    bank.status = status ? 'active' : 'inactive'

                    bank.save((error, bank) => {
                        if (error) {
                            response.send(utils.createError(error))
                        } else if (!bank) {
                            response.send(utils.createError('bank not found'))
                        } else {
                            response.send(utils.createSuccess(bank));
                        }
                    })
                }
            })
        } else {
            response.send(utils.createError('invalid request'))
        }
    } catch (ex) {
        console.log(ex)
        response.send(utils.createError('Unauthorized: invalid token'))
    }
})

module.exports = router;