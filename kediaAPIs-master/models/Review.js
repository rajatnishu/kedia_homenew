const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const ReviewSchema = Schema({    
    property: { type: mongoose.Schema.Types.ObjectId, ref: 'Property' },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    comment: { type: String, required: false, trim: true},
    rating: { type: Number, required: true, trim: true},  
    
    deleted: { type: Boolean, default: false, trim: true },
    createdOn: { type: Date, default: new Date(), trim: true }
});


ReviewSchema.pre('find', function() {    
    this.populate({path:'user',select:'firstName lastName phone isdCode email role image'});
  });

module.exports = mongoose.model('Review', ReviewSchema);