const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const ImageSchema = new Schema({
    image: { type: String, default: '', trim: true },
    status: { type: String, default: 'active', trim: true },
    type: { type: String, default: '', trim: true },
    heading: { type: String, default: '', trim: true },
    projectLink: { type: String, default: '', trim: true },
    deleted: { type: Boolean, default: false, trim: true }
}, {
    timestamps: true
});


module.exports = mongoose.model('Image', ImageSchema);