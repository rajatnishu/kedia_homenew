const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
    device_token: { type: String, required: false, trim: true },
    fullName: { type: String, required: false, trim: true },
    phone: { type: String, required: false, trim: true, index: { unique: true } },
    isdCode: { type: Number, required: false, defailt: '+91', trim: true },
    email: { type: String, required: false, trim: true },
    password: { type: String, required: false, trim: true },
    gender: { type: String, default: 'male', trim: true },
    role: { type: String, default: 'user', required: false, trim: true },
    image: { type: String, default: 'user.png', trim: true },

    location: {
        type: { type: String, default: "Point" },
        coordinates: [{ type: Number }],
        label: { type: String, default: "Home" },
    },

    aadharCard: { type: String, default: 'user.png', trim: true },

    city: { type: String, default: '', trim: true },
    state: { type: String, default: '', trim: true },
    country: { type: String, default: '', trim: true },

    emailVerified: { type: Boolean, default: false, trim: true },
    phoneVerified: { type: Boolean, default: false, trim: true },

    provider: { type: String, default: "WEB", trim: true },

    access_token: { type: String, default: "", trim: true },
    otp: { type: String, default: "", trim: true },
    otpVerified: { type: Boolean, default: false, trim: true },
    status: { type: Boolean, default: true, trim: true },
    deleted: { type: Boolean, default: false, trim: true },

}, { timestamps: true })



UserSchema.methods.safeUser = function () {
    return {
        _id: this._id,
        fullName: this.fullName,
        phone: this.phone,
        isdCode: this.isdCode,
        phoneVerified: this.phoneVerified,
        role: this.role,
        image: this.image,
        aadharCard: this.aadharCard,
        location: this.location,
        access_token: this.access_token,
        updatedAt: this.updatedAt,
        createdAt: this.createdAt,
        status: this.status
    };
};


let userModel = mongoose.model('User', UserSchema)

module.exports = mongoose.model('User', UserSchema)
