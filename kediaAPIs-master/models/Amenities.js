const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const AmenitiesSchema = new Schema({
    name: { type: String, default: '', trim: true },
    logo: { type: String, default: '', trim: true },
    status: { type: String, default: 'active', trim: true },

    deleted: { type: Boolean, default: false, trim: true }
}, {
    timestamps: true
});


module.exports = mongoose.model('Amenity', AmenitiesSchema);