const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const BankSchema = new Schema({
    name: { type: String, default: '', trim: true },
    logo: { type: String, default: '', trim: true },
    loanPercent: { type: String, default: '', trim: true },
    status: { type: String, default: 'active', trim: true },

    deleted: { type: Boolean, default: false, trim: true }
}, {
    timestamps: true
});


BankSchema.pre('find', function () {
    this.populate({ path: 'user', select: 'firstName lastName phone isdCode email role image' });
});

module.exports = mongoose.model('Bank', BankSchema);