const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const BuildingSchema = new Schema({    
    name: {type:String, required:true,trim: true},
    description: {type:String, default: "",trim: true},
    project: { type: mongoose.Schema.Types.ObjectId, ref: 'Project'},
    floors: { type: Number, default: 0, trim:true},
    flats: { type: Number, default: 0, trim:true},
    lifts: { type: Number, default: 0, trim:true},
    serviceLift: { type: Boolean, default:false, trim:true},

    images: [{type:String, default: "",trim: true}],
    
    published:{ type: Boolean, default: false, trim: true},
    deleted: {type: Boolean, default:false, trim:true}
},{timestamps:true});


module.exports = mongoose.model('Building', BuildingSchema);