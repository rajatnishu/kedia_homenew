const mongoose = require('mongoose')

const Schema = mongoose.Schema;


const Statuses = {
  NEW: 0,
  PUBLISHED: 1,
  BOOKED: 2
};

const PropertySchema = new Schema({
  type: {
    type: String,
    enum: {
      values: ["Plot", "Flat", "Villa", "Row House"],
      message:
        "Type can be either: Plot, Villa, Row House or Flat.",
    },
    required: [true, "Type is required."],
    trim: true
  },
  description: { type: String, default: "", trim: true },
  areaSqft: { type: Number, required: [true, 'Area in Sqft. is required'], trim: true },
  serial: { type: String, required: [true, 'Property Serial Number is required'], trim: true },
  planImage: { type: String, default: "", trim: true },
  images: [{ type: String, default: "", trim: true }],
  bedrooms: { type: Number, default: 0, trim: true },
  bathrooms: { type: Number, default: 0, trim: true },
  livingRoom: { type: Number, default: 0, trim: true },
  balcony: { type: String, default: false, trim: true },
  facing: {
    type: String,
    enum: {
      values: ["East", "South", "West", "North", ""],
      message:
        "Type can be either: East, West, North or South.",
    },
    default: "",
    trim: true
  },

  possessionStatus: { type: String, default: false, trim: true },
  perSqFtPrice: { type: String, default: false, trim: true },
  images360: [{ type: String, default: "", trim: true }],

  price: { type: Number, default: 0, trim: true },
  bookingPrice: { type: Number, default: 0, trim: true },
  floor: { type: Number, default: 0, trim: true },
  building: { type: mongoose.Schema.Types.ObjectId, ref: 'Building' },
  project: { type: mongoose.Schema.Types.ObjectId, ref: 'Project' },

  status: { type: Number, default: 0, trim: true },
  isBooked: { type: Boolean, default: false, trim: true },
  bookedId: { type: mongoose.Schema.Types.ObjectId, ref: 'Booking' },
  published: { type: Boolean, default: false, trim: true },
  deleted: { type: Boolean, default: false, trim: true },
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },

}, { timestamps: true });

module.exports = mongoose.model('Property', PropertySchema);