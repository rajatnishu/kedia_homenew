const mongoose = require('mongoose')
const Schema = mongoose.Schema

const BookingSchema = new Schema({
    name: { type: String, required: false, trim: true },
    phone: { type: String, required: false, trim: true },
    timing: { type: String, required: false, trim: true, get: getTiming },
    date: { type: Date, required: false, trim: true },

    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    property: { type: mongoose.Schema.Types.ObjectId, ref: 'Property' },

    currency: { type: String, required: false, trim: true },
    totalAmount: { type: Number, required: false, trim: true },
    paidAmount: { type: Number, required: false, trim: true },

    promoDiscount: { type: Number, required: false, trim: true },
    pendingAmount: { type: Number, required: false, trim: true },
    paymentOption: { type: String, required: false, trim: true },

    transactionId: { type: String, required: false, trim: true },
    transactionStatus: { type: String, default: '0', trim: true },
    transactionAmount: { type: String, required: false, trim: true },
    responseObject: { type: Object, required: false, trim: true },

    ticket: { type: String, required: false, trim: true },

    payment: { type: String, default: "pending", trim: true },
    isOffline: { type: Boolean, default: false, trim: true },
    status: { type: String, default: "active", trim: true },
    deleted: { type: Boolean, default: false, trim: true },
    createdOn: { type: Date, default: new Date(), trim: true }
}, { timestamps: true })


BookingSchema.pre('validate', function (next) {
    console.log("pre", this)
    // Only increment when the document is new
    if (this.isNew) {
        bookingModel.countDocuments().then(res => {
            let rand = Math.floor(Math.random() * 9);
            this.ticket = `KPB${21111110 + rand + res * 10}`; // Increment count
            next();
        });
    } else {
        next();
    }
});

function getTiming(timing) {
    return (timing && timing.split(" ").join("")) || "";
}

BookingSchema.set('toObject', { getters: true, setters: true, virtuals: true });
BookingSchema.set('toJSON', { getters: true, setters: true, virtuals: true });

let bookingModel = mongoose.model('Booking', BookingSchema);

module.exports = mongoose.model('Booking', BookingSchema);

// ticket:{type:String,default:function getNextSequence() {
//     var ret = db.counters.findOneAndModify({
//                 query: { model:'booking' },
//                 update: { $inc: { sequence: 10 } }                    
//             }
//     );
//     return ret.seq;
//     }
// },