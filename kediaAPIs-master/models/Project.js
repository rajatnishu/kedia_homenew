const { string } = require('@hapi/joi');
const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const ProjectSchema = new Schema({
    name: { type: String, required: true, trim: true },
    description: { type: String, default: "", trim: true },
    amenities: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Amenity' }],

    images: [{ type: String, default: "", trim: true }],

    logo: [{ type: String, default: "", trim: true }],
    area: { type: String, default: '', trim: true },
    launchDate: { type: String, default: '', trim: true },
    reraId: { type: String, default: '', trim: true },
    bankDetails: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Bank' }],

    startsFrom: { type: String, default: '', trim: true },

    category: { type: String, default: 'new', trim: true },
    keyfeatures: [{ type: String, default: '', trim: true }],
    whyInvest: [{ type: String, default: '', trim: true }],
    bhkNums: [{ type: Object, default: {}, trim: true }],
    planMap: { type: String, default: '', trim: true },
    broucher: { type: String, default: '', trim: true },
    floorings: [{ type: String, default: 'new', trim: true }],
    fittings: [{ type: String, default: 'new', trim: true }],
    walls: [{ type: String, default: 'new', trim: true }],
    avgPrice: { type: String, default: '', trim: true },
    typesOfFlats: { type: String, default: '', trim: true },
    builtUpArea: { type: String, default: '', trim: true },

    location: {
        type: { type: String, default: "Point" },
        coordinates: [{ type: Number }]
    },
    numberOfBuildings: { type: Number, default: 0, trim: true },
    numberOfHouses: { type: Number, default: 0, trim: true },
    address: { type: String, default: "", trim: true },
    city: { type: String, default: "", trim: true },
    state: { type: String, default: "", trim: true },
    country: { type: String, default: "India", trim: true },

    published: { type: Boolean, default: false, trim: true },
    deleted: { type: Boolean, default: false, trim: true }
}, { timestamps: true });



ProjectSchema.index({ location: "2dsphere" });
module.exports = mongoose.model('Project', ProjectSchema);