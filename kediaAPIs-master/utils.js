const jwt = require('jsonwebtoken');
const config = require('./config/constants');
const Request = require("request")
const User = require('./models/User')


// create result
function createResult(error, data, count) {
    const result = {};
    if (error == null || error == undefined) {
        result['status'] = 'success'
        result['data'] = data
        result['count'] = count
        // result['pageSize'] = config.pageSize
    } else {
        result['status'] = 'error';
        if (typeof (error) == 'string') {
            result['error'] = error;
        } else {
            result['error'] = `${error}`;
        }

    }

    return result;
}

// create error
function createError(message) {
    return createResult(message, message);
}

// create success message
function createSuccess(message) {
    return createResult(null, message);
}

// get pages
function getPages(count) {
    return Math.ceil(count / config.pageSize)
}

function authorize(request, response, next) {
    const token = request.headers['X-Auth-Token'];
    let result = {};
    if (token != undefined) {
        try {
            const data = jwt.verify(token, config.secret);
            result = createResult(null, data);
            next();
        } catch (ex) {
            result = createError('Invalid token');
        }
    } else {
        result = createError('Token missing');
    }

    response.send(result);
}

function createDate(year, month, day) {
    return ""
}

function parseDate(strDate) {
    const parts = strDate.split('-');
    console.log(parts)
    const date = new Date(new Date(parseInt(parts[0]), parseInt(parts[1]) - 1, parseInt(parts[2])));
    console.log(date)
    return date;
}

function parseTime(strDate, strTime) {
    const dateParts = strDate.split('-')
    const timeParts = strTime.split(':')
    const timestamp = new Date(
        parseInt(dateParts[0]),
        parseInt(dateParts[1]) - 1,
        parseInt(dateParts[2]),
        parseInt(timeParts[0]),
        parseInt(timeParts[1])
    )
    const date = new Date(timestamp);
    return date;
}

function randomString(length = 6) {
    const chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    let randomstring = '';
    for (let i = 0; i < length; i++) {
        const rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}

function randomNumber(length = 6) {
    const chars = '0123456789';
    let randomstring = '';
    for (let i = 0; i < length; i++) {
        const rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}


function makeRequest(url) {
    return new Promise((resolve, reject) => {
        Request(url, (error, response, body) => {
            if (error) {
                reject(error)
            }
            resolve(JSON.parse(body));
        });
    });
}

function makePostRequest(url, body) {
    return new Promise((resolve, reject) => {
        var postheaders = {
            'Content-Type': 'application/json'
        };
        const options = {
            url: url,
            body: body,
            json: true,
            method: 'POST',
            headers: postheaders
        }
        Request(options, (error, response, body) => {
            if (error) {
                reject(error)
            }
            console.log(body)
            resolve(body);
        });
    });
}

function getSlots(timeRange) {
    console.log("timeRange: ", timeRange)
    let timesArr = timeRange.split('-')
    let startTime = timesArr[0].substr(0, timesArr[0].length - 3).split(':');
    let endTime = timesArr[1].substr(0, timesArr[1].length - 3).split(':');
    console.log(timesArr, startTime, endTime);

}


function verifyAdmin(request, response, next) {
    const token = request.headers['token']
    console.log('token:', token)
    if (token == undefined) {
        response.status(403)
        response.send('unauthorized')
        return
    } else {
        try {
            const data = jwt.verify(token, config.secret)
            console.log(data)
            request.userId = data.id
            User.findOne({ _id: data.id }).exec((err, user) => {
                if (user) {
                    request.username = user.username
                    request.userrole = user.role || " "
                    if (user.role === 'admin')
                        next()
                    else {
                        response.status(401)
                        response.send('invalid token')
                    }
                } else {
                    response.status(401)
                    response.send('invalid token')
                }
            })

        } catch (ex) {
            response.status(401)
            response.send('invalid token')
        }
    }
}
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
module.exports = {
    createResult: createResult,
    createError: createError,
    createSuccess: createSuccess,
    authorize: authorize,
    parseDate: parseDate,
    parseTime: parseTime,
    randomString: randomString,
    randomNumber: randomNumber,
    makeRequest: makeRequest,
    makePostRequest: makePostRequest,
    getPageCount: getPages,
    getSlots: getSlots,
    validateEmail: validateEmail,
    verifyAdmin: verifyAdmin
};