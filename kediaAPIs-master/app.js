const mongoose = require('mongoose')
const express = require('express')
const path = require('path');
const logger = require('morgan')
const cors = require('cors')
const jwt = require('jsonwebtoken')
const dbConfig = require('./config/database')
const config = require('./config/constants')
const fs = require('fs')
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser')

const indexRouter = require('./routes/index')
const projectsRouter = require('./routes/projects')
const bookingsRouter = require('./routes/bookings')
const usersRouter = require('./routes/users')
const bankRouter = require('./routes/banks')
const amenitiesRouter = require('./routes/amenities')
const imagesRouter = require('./routes/images')

const app = express()
app.use(cors())
app.options('*', cors())

app.use((request, response, next) => {
    console.log("------------------------------- New Request ------------------------------------")
    console.log("For Path: ", request.url)
    if (
        (request.url.toLowerCase().startsWith('/app')) ||
        (!request.url.toLowerCase().startsWith('/api')) ||
        (request.url.toLowerCase().startsWith('/api/v1/user/socialauth/')) ||
        (request.url.toLowerCase().startsWith('/api/v1/user/checkusername/')) ||
        (request.url.toLowerCase().startsWith('/uploads/')) ||
        (request.url.toLowerCase() === '/api/v1/country/list') ||
        (request.url.toLowerCase() === '/api/v1/user/login') ||
        (request.url.toLowerCase() === '/api/v1/user/register') ||
        (request.url.toLowerCase() === '/api/v1/user/verify-otp') ||
        (request.url.toLowerCase() === '/api/v1/user/forgot-password') ||
        (request.url.toLowerCase() === '/api/v1/user/reset-password') ||

        (request.url.toLowerCase().startsWith('/api/v1/project')) ||
        (request.url.toLowerCase() === '/api/v1/images/list')
    ) {
        next()
    } else if (
        (!request.url.toLowerCase().startsWith('/api'))
    ) {
        next()
    } else {
        const token = request.headers['token']
        console.log('token:', token)
        if (token == undefined) {
            response.status(403)
            response.send('unauthorized')
            return
        } else {
            try {
                const data = jwt.verify(token, config.secret)
                console.log(data)
                request.userId = data.id
                request.username = data.username
                request.userrole = data.role || ""
                next()
            } catch (ex) {
                response.status(401)
                response.send('invalid token')
            }
        }
    }
})


mongoose.connect(dbConfig, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true });

app.use(logger('dev'))
app.use(express.json())
app.use(bodyParser.urlencoded({
    extended: true,
    parameterLimit: 50000
}));

app.get('/data', (req, res) => {
    res.send("api working fine")
  })
  
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', express.static('public/app'))

app.use((request, response, next) => {
    const size = config.pageSize
    request.pagination = {}
    let page = 1
    if (request.query.page) {
        page = parseInt(request.query.page)
    }
    request.pagination.skip = size * (page - 1)
    request.pagination.limit = size
    console.log("request.pagination", request.pagination)
    console.log("request.body", request.body)
    console.log("request.params", request.params)
    next()
})

app.use('/api/v1/', indexRouter)
app.use('/api/v1/user', usersRouter)
app.use('/api/v1/project', projectsRouter)
app.use('/api/v1/booking', bookingsRouter)
app.use('/api/v1/bank', bankRouter)
app.use('/api/v1/amenities', amenitiesRouter)
app.use('/api/v1/images', imagesRouter)

module.exports = app
